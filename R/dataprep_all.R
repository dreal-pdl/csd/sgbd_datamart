# Fonction pour la source RPLS adaptée de puis https://gitlab.com/rdes_dreal/propre.rpls/-/blob/master/R/dataprep.R?ref_type=heads

#' Phase de datapreparation pour la publication
#'
#' @description Consolidation des indicateurs aux differentes échelles 
#'
#' @param test TRUE si on souhaite tester dataprep() sur 100 communes choisies au hasard.
#'
#' @return Un dataframe d'indicateurs : une ligne par entite geographique et millesime
#' @importFrom COGiter cogifier ajouter_zonage
#' @importFrom dplyr select full_join mutate vars starts_with mutate filter case_when across pull group_by ungroup lag arrange
#' @importFrom forcats fct_relevel
#' @importFrom rlang .data
#' @importFrom tidyr replace_na expand_grid
#' @export
#'

dataprep_all <- function(test = FALSE) {
  
  if (test) {
    liste_depcom <- propre.rpls::tab_result %>%
      dplyr::pull("DEPCOM") %>%
      sample(size = 100)
    tab_result <- propre.rpls::tab_result %>%
      dplyr::filter(.data$DEPCOM %in% liste_depcom) %>%
      dplyr::mutate(millesime = factor(.data$millesime),
                    DEPCOM = factor(.data$DEPCOM))
    lgt_rp <- propre.rpls::lgt_rp %>%
      dplyr::filter(DEPCOM %in% liste_depcom)
  } else {
    liste_depcom <- COGiter::communes %>% dplyr::pull("DEPCOM")
    tab_result <- propre.rpls::tab_result %>%
      dplyr::mutate(millesime = factor(.data$millesime),
                    DEPCOM = factor(.data$DEPCOM))
    lgt_rp <- propre.rpls::lgt_rp
  }
  
  mil_disp <- dplyr::select(tab_result, "millesime") %>%
    dplyr::pull("millesime") %>%
    unique()
  # Le df de départ, une ligne par commune à jour et millésime
  df <- tidyr::expand_grid(DEPCOM = liste_depcom, millesime = mil_disp)
  
  # un df intermediaire pour pouvoir calculer les indicateurs de densite LS / RP pour toutes les annees
  # (on duplique les données du RP pour chaque millesime disponible)
  lgt_rp_mil <- dplyr::full_join(df, lgt_rp, by = "DEPCOM") %>%
    dplyr::select(-"mil_RP", -"mil_RP_old", -"nb_an_evol_rp")
  # une variable globale pour le nb d'annees d'ecart entre les mil du RP les plus récents pour le calcul des evolutions comparées RP/RPLS dans un dplyr::lag
  ecart_an_rp <- lgt_rp$nb_an_evol_rp[1]
  
  # etape de construction du résultat :
  # 1 - pour les indicateurs non sommables, on recalcule les composantes sommables
  # 2 - pour les indicateurs à NA, on complète à 0
  # 3 - passage des données tab_result sur le COG 2020
  # 4 - fusion avec le fichier rp complet sur la liste des communes
  # 5 - pour les communes non présentes dans tab_result, on met les indicateurs à 0
  # 6 - cogification
  # 7 - filtrage des données sur la région
  # 8 - calcul des indicatrices pertinentes pour filtrer les données ensuite : Zone_ref et select_prov_drom
  # 9 - recalcule des indicateurs non sommables
  # 10 - suppression des variables non nécessaires
  res <- tab_result %>%
    dplyr::mutate(dplyr::across(where(is.numeric), ~ tidyr::replace_na(., 0))) %>%
    COGiter::passer_au_cog_a_jour(garder_info_supra = FALSE) %>%
    dplyr::full_join(lgt_rp_mil, by = c("DEPCOM", "millesime")) %>%
    dplyr::mutate(dplyr::across(where(is.numeric), ~ tidyr::replace_na(., 0))) %>%
    COGiter::cogifier(
      communes = TRUE, epci = TRUE, departements = TRUE, regions = TRUE,
      metro = TRUE, metrodrom = TRUE, franceprovince = TRUE, drom = TRUE) %>%
    # COGiter::ajouter_zonage(zonage_df = propre.rpls::zonage_spe %>% dplyr::select(-"REG")) %>%
    # dplyr::filter(!(TypeZone %in% c("Communes","D\u00e9partements", "EPT")) | CodeZone %in% communes_departement_a_garder)  %>%
    # dplyr::mutate(
    #   Zone_ref = dplyr::case_when(
    #     .data$TypeZone == "R\u00e9gions" & .data$CodeZone == id_reg ~ TRUE,
    #     .data$TypeZone == "D\u00e9partements" ~ TRUE,
    #     .data$TypeZone == "France" ~ TRUE,
    #     .data$TypeZone == "EPT" ~ TRUE,
    #     .data$CodeZone %in% epci_choisis ~ TRUE,
    #     TRUE ~ FALSE),
    #   select_prov_drom = dplyr::case_when(
  #     id_reg %in% paste0("0", 1:6) & .data$CodeZone %in% c("FRMETRO", "FRPROV") ~ FALSE,
  #     id_reg %in% paste0("0", 1:6) & .data$TypeZone == "D\u00e9partements" ~ FALSE,
  #     !(id_reg %in% paste0("0", 1:6)) & .data$CodeZone %in% c("DROM", "FRMETRODROM") ~ FALSE,
  #     TRUE ~ TRUE),
  #   TypeZone = forcats::fct_relevel(.data$TypeZone, "Communes", "Epci", "EPT", "D\u00e9partements", "R\u00e9gions", "France"),
  #   # Mettre au bon endroit les levels ajoutes avec ajouter_zonage
  #   CodeZone = forcats::fct_relevel(.data$CodeZone, sort)
  # ) %>%
  # dplyr::filter(.data$select_prov_drom) %>%
  dplyr::arrange(.data$TypeZone,.data$Zone,.data$CodeZone,.data$millesime) %>%
    dplyr::group_by(.data$TypeZone,.data$Zone,.data$CodeZone)
  res2 <- res %>%
    # reconstitution des variables intensives
    dplyr::mutate(
      age_moyen =  .data$age_somme / .data$nb_ls_age_connu,
      age_moyen_qpv = .data$age_somme_qpv / .data$nb_ls_qpv_age_connu,
      part_dpe_realise = .data$nb_ls_dpe_realise / .data$nb_ls_actif * 100,
      age_dpe_ener_A = .data$age_somme_dpe_ener_A / .data$nb_ls_age_connu_dpe_ener_A * 100,
      age_dpe_ener_B = .data$age_somme_dpe_ener_B / .data$nb_ls_age_connu_dpe_ener_B * 100,
      age_dpe_ener_C = .data$age_somme_dpe_ener_C / .data$nb_ls_age_connu_dpe_ener_C * 100,
      age_dpe_ener_D = .data$age_somme_dpe_ener_D / .data$nb_ls_age_connu_dpe_ener_D * 100,
      age_dpe_ener_E = .data$age_somme_dpe_ener_E / .data$nb_ls_age_connu_dpe_ener_E * 100,
      age_dpe_ener_F = .data$age_somme_dpe_ener_F / .data$nb_ls_age_connu_dpe_ener_F * 100,
      age_dpe_ener_G = .data$age_somme_dpe_ener_G / .data$nb_ls_age_connu_dpe_ener_G * 100,
      age_dpe_ener_new_A = .data$age_somme_dpe_ener_new_A / .data$nb_ls_age_connu_dpe_ener_new_A * 100,
      age_dpe_ener_new_B = .data$age_somme_dpe_ener_new_B / .data$nb_ls_age_connu_dpe_ener_new_B * 100,
      age_dpe_ener_new_C = .data$age_somme_dpe_ener_new_C / .data$nb_ls_age_connu_dpe_ener_new_C * 100,
      age_dpe_ener_new_D = .data$age_somme_dpe_ener_new_D / .data$nb_ls_age_connu_dpe_ener_new_D * 100,
      age_dpe_ener_new_E = .data$age_somme_dpe_ener_new_E / .data$nb_ls_age_connu_dpe_ener_new_E * 100,
      age_dpe_ener_new_F = .data$age_somme_dpe_ener_new_F / .data$nb_ls_age_connu_dpe_ener_new_F * 100,
      age_dpe_ener_new_G = .data$age_somme_dpe_ener_new_G / .data$nb_ls_age_connu_dpe_ener_new_G * 100,
      densite_ls_rp = .data$nb_ls_actif * 100 / .data$nb_rp,
      densite_ls_loues_rp = .data$nb_ls_loue * 100 / .data$nb_rp,
      evolution_rp =  (.data$nb_rp - .data$nb_rp_old) * 100 / .data$nb_rp_old,
      evolution_n_nmoins1 =  (.data$nb_ls_actif - dplyr::lag(.data$nb_ls_actif)) * 100 / dplyr::lag(.data$nb_ls_actif),
      evolution_annees_rp = (.data$nb_ls_actif - dplyr::lag(.data$nb_ls_actif, ecart_an_rp)) * 100 / dplyr::lag(.data$nb_ls_actif, ecart_an_rp),
      nb_ls_recent = .data$nb_piece_1_recent + .data$nb_piece_2_recent + .data$nb_piece_3_recent + .data$nb_piece_4_recent + .data$nb_piece_5_plus_recent,
      part_ls_qpv = .data$nb_ls_qpv / .data$nb_ls_actif * 100,
      part_ls_ind = .data$nb_ls_ind / .data$nb_ls_actif * 100,
      part_ls_coll = .data$nb_ls_coll / .data$nb_ls_actif * 100,
      part_ls_etu = .data$nb_ls_etu / .data$nb_ls_actif * 100,
      part_ls_oph = .data$nb_ls_oph / .data$nb_ls_actif * 100,
      part_ls_esh = .data$nb_ls_esh / .data$nb_ls_actif * 100,
      part_ls_sem = .data$nb_ls_sem / .data$nb_ls_actif * 100,
      part_ls_loue = .data$nb_ls_loue / .data$nb_ls_actif * 100,
      part_ls_vacant = .data$nb_ls_vacant / .data$nb_ls_actif * 100,
      part_ls_vide = .data$nb_ls_vide / .data$nb_ls_actif * 100,
      part_ls_association = .data$nb_ls_association / .data$nb_ls_actif * 100,
      part_ls_autre = .data$nb_ls_autre / .data$nb_ls_actif * 100,
      part_ls_recent =  .data$nb_ls_recent / .data$nb_ls_actif * 100,
      part_ls_ind_recent = .data$nb_ls_ind_recent / .data$nb_ls_recent * 100,
      part_ls_coll_recent = .data$nb_ls_coll_recent / .data$nb_ls_recent * 100,
      part_ls_etu_recent =  .data$nb_ls_etu_recent / .data$nb_ls_recent * 100,
      part_ls_qpv_ind = .data$nb_ls_ind_qpv / .data$nb_ls_qpv * 100,
      part_ls_qpv_coll = .data$nb_ls_coll_qpv / .data$nb_ls_qpv * 100,
      part_ls_qpv_etu = .data$nb_ls_etu_qpv / .data$nb_ls_qpv * 100,
      part_ls_1p = .data$nb_piece_1 / .data$nb_ls_actif * 100,
      part_ls_recent_1p = .data$nb_piece_1_recent / .data$nb_ls_recent * 100,
      part_ls_2p = .data$nb_piece_2 / .data$nb_ls_actif * 100,
      part_ls_recent_2p = .data$nb_piece_2_recent / .data$nb_ls_recent * 100,
      part_ls_3p = .data$nb_piece_3 / .data$nb_ls_actif * 100,
      part_ls_recent_3p = .data$nb_piece_3_recent / .data$nb_ls_recent * 100,
      part_ls_4p = .data$nb_piece_4 / .data$nb_ls_actif * 100,
      part_ls_recent_4p = .data$nb_piece_4_recent / .data$nb_ls_recent * 100,
      part_ls_5pp = .data$nb_piece_5_plus / .data$nb_ls_actif * 100,
      part_ls_recent_5pp = .data$nb_piece_5_plus_recent / .data$nb_ls_recent * 100,
      part_mes_qpv = .data$nb_mes_qpv / .data$nb_mes * 100,
      part_ls_age_0_4 = .data$nb_ls_age_0_5 / .data$nb_ls_actif * 100,
      part_ls_age_5_9 = .data$nb_ls_age_5_10 / .data$nb_ls_actif * 100,
      part_ls_age_10_19 = .data$nb_ls_age_10_20 / .data$nb_ls_actif * 100,
      part_ls_age_20_39 = .data$nb_ls_age_20_40 / .data$nb_ls_actif * 100,
      part_ls_age_40_59 = .data$nb_ls_age_40_60 / .data$nb_ls_actif * 100,
      part_ls_age_60p = .data$nb_ls_age_60_plus / .data$nb_ls_actif * 100,
      nb_ls_loues_proploc =  .data$nb_ls_loue + .data$nb_ls_vacant,
      taux_vacance_tot = .data$nb_ls_vacant / .data$nb_ls_loues_proploc * 100,
      taux_vacance_str = .data$nb_ls_vacant_3 / .data$nb_ls_loues_proploc * 100,
      taux_mobilite = .data$num_mob / .data$denom_mob *100,
      taux_mobilite_1p = .data$num_mob_1_piece / .data$denom_mob_1_piece *100,
      taux_mobilite_2p = .data$num_mob_2_piece / .data$denom_mob_2_piece *100,
      taux_mobilite_3p = .data$num_mob_3_piece / .data$denom_mob_3_piece *100,
      taux_mobilite_4p = .data$num_mob_4_piece / .data$denom_mob_4_piece *100,
      taux_mobilite_5pp = .data$num_mob_5_piece / .data$denom_mob_5_piece *100,
      taux_vacance_str_0_4ans = .data$num_vac_struct_age_0_5 / .data$denom_vac_struct_age_0_5 * 100,
      taux_vacance_str_5_9ans = .data$num_vac_struct_age_5_10 / .data$denom_vac_struct_age_5_10 * 100,
      taux_vacance_str_10_19ans = .data$num_vac_struct_age_10_20 / .data$denom_vac_struct_age_10_20 * 100,
      taux_vacance_str_20_39ans = .data$num_vac_struct_age_20_40 / .data$denom_vac_struct_age_20_40 * 100,
      taux_vacance_str_40_59ans = .data$num_vac_struct_age_40_60 / .data$denom_vac_struct_age_40_60 * 100,
      taux_vacance_str_60ans_plus = .data$num_vac_struct_age_60_plus / .data$denom_vac_struct_age_60_plus * 100,
      taux_vacance_str_qpv = .data$num_vac_struct_qpv / .data$denom_vac_struct_qpv * 100,
      taux_vacance_str_hors_qpv = .data$num_vac_struct_horsqpv / .data$denom_vac_struct_horsqpv * 100,
      part_mes_plai = .data$nb_plai / .data$nb_mes * 100,
      part_mes_plus = .data$nb_plus / .data$nb_mes * 100,
      part_mes_pls = .data$nb_pls / .data$nb_mes * 100,
      part_mes_pli = .data$nb_pli / .data$nb_mes * 100,
      loyer_m2_plai = .data$somme_loyer_nb_plai / .data$somme_surface_nb_plai,
      loyer_m2_plus = .data$somme_loyer_nb_plus / .data$somme_surface_nb_plus,
      loyer_m2_pls = .data$somme_loyer_nb_pls / .data$somme_surface_nb_pls,
      loyer_m2_pli = .data$somme_loyer_nb_pli / .data$somme_surface_nb_pli,
      loyer_m2_plai_recent = .data$somme_loyer_nb_plai_recent / .data$somme_surface_nb_plai_recent,
      loyer_m2_plus_recent = .data$somme_loyer_nb_plus_recent / .data$somme_surface_nb_plus_recent,
      loyer_m2_pls_recent = .data$somme_loyer_nb_pls_recent / .data$somme_surface_nb_pls_recent,
      loyer_m2_pli_recent = .data$somme_loyer_nb_pli_recent / .data$somme_surface_nb_pli_recent,
      loyer_m2_plai_mes = .data$somme_loyer_nb_plai_mes / .data$somme_surface_nb_plai_mes,
      loyer_m2_plus_mes = .data$somme_loyer_nb_plus_mes / .data$somme_surface_nb_plus_mes,
      loyer_m2_pls_mes = .data$somme_loyer_nb_pls_mes / .data$somme_surface_nb_pls_mes,
      loyer_m2_pli_mes = .data$somme_loyer_nb_pli_mes / .data$somme_surface_nb_pli_mes,
      loyer_m2 = .data$somme_loyer / .data$somme_surface,
      loyer_m2_recent = .data$somme_loyer_recent / .data$somme_surface_recent,
      loyer_m2_qpv = .data$somme_loyer_enqpv / .data$somme_surface_enqpv,
      loyer_m2_mes = (.data$somme_loyer_nb_plai_mes + .data$somme_loyer_nb_plus_mes + .data$somme_loyer_nb_pls_mes + .data$somme_loyer_nb_pli_mes ) /
        (.data$somme_surface_nb_plai_mes + .data$somme_surface_nb_plus_mes + .data$somme_surface_nb_pls_mes + .data$somme_surface_nb_pli_mes),
      loyer_m2_0_4ans = .data$somme_loyer_age_inf_5 / .data$somme_surface_age_inf_5,
      loyer_m2_5_9ans = .data$somme_loyer_age_5_10 / .data$somme_surface_age_5_10,
      loyer_m2_10_19ans = .data$somme_loyer_age_10_20 / .data$somme_surface_age_10_20,
      loyer_m2_20_39ans = .data$somme_loyer_age_20_40 / .data$somme_surface_age_20_40,
      loyer_m2_40_59ans = .data$somme_loyer_age_40_60 / .data$somme_surface_age_40_60,
      loyer_m2_60ans_et_plus = .data$somme_loyer_age_60_plus / .data$somme_surface_age_60_plus,
      part_mes_qpv_plai = .data$nb_mes_plai_qpv / .data$nb_mes_qpv * 100,
      part_mes_qpv_plus = .data$nb_mes_plus_qpv / .data$nb_mes_qpv * 100,
      part_mes_qpv_pls = .data$nb_mes_pls_qpv / .data$nb_mes_qpv * 100,
      part_mes_qpv_pli = .data$nb_mes_pli_qpv / .data$nb_mes_qpv * 100,
      part_plai_acq_av_travaux = .data$nb_plai_acq_av_travaux / .data$nb_plai * 100,
      part_plai_acq_ss_travaux = .data$nb_plai_acq_ss_travaux / .data$nb_plai * 100,
      part_plai_acq_vefa = .data$nb_plai_acq_vefa / .data$nb_plai * 100,
      part_plai_construit_org = .data$nb_plai_construit_org / .data$nb_plai * 100
    ) %>%
    dplyr::ungroup()
  
  return(res2)
  
}
