
reg <- 52

com_reg_et_vois <- dplyr::filter(
  COGiter::communes, 
  grepl(reg, REGIONS_DE_L_EPCI)
  ) %>%
  dplyr::select(DEPCOM) %>%
  dplyr::mutate(DEPCOM = as.character(DEPCOM)) %>%
  dplyr::pull(DEPCOM)

com_reg <- dplyr::filter(
  COGiter::communes, 
  REG == reg
  ) %>%
  dplyr::select(DEPCOM) %>%
  dplyr::mutate(DEPCOM = as.character(DEPCOM)) %>%
  dplyr::pull(DEPCOM)

