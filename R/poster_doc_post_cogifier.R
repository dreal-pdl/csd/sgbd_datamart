library(datalibaba)
library(googlesheets4)
library(dplyr)

# une fonction pour propager les commentaires de tables et de champs à la table cogifiée

poster_documenter_post_cogifier <- function(source = "combustible_principal_rp") {
  # creation du nom des tables
  tbl_source <- paste0("source_", source)
  tbl_cogifiee <- paste0("cogifiee_", source)
  
  # récupération des commentaires de la table source
  dico_var <- get_table_comments(
    db = "datamart",
    schema = "portrait_territoires",
    table = tbl_source,
    user = "does")
  
  # commentaire de la table
  comm_table <- filter(dico_var, is.na(nom_col)) %>% 
    pull(commentaire) %>% 
    gsub("\nCommentaire.*$", "", .)
  
  commenter_table(
    comment = comm_table,
    db = "datamart",
    schema = "portrait_territoires",
    table = tbl_cogifiee,
    user = "does"
  )
  
  # commentaire des variables
  comm_champ <- select(dico_var, nom_col, commentaire) %>% 
    filter(!is.na(nom_col), nom_col != "depcom") %>% 
    bind_rows(
      tribble(
        ~nom_col, ~commentaire,
        "TypeZone", "Type de territoire",
        "Zone", " Nom du territoire",
        "CodeZone", "Code INSEE du territoire"
      )
    )
  
  post_dico_attr(
    dico = comm_champ,
    db = "datamart",
    schema = "portrait_territoires",
    table = tbl_cogifiee,
    user = "does"
  )
  
}
