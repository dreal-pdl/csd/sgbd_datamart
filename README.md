Scripts d'alimentation du datamart de l'application Indicateurs territoriaux.     
Code source de l'application : https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/it_indicateurs   
Lien vers l'application en production : http://apps.datalab.pays-de-la-loire.developpement-durable.gouv.fr/indicateurs_territoriaux/
Lien vers la version en coutrs de développement (intranet) : http://set-pdl-dataviz.dreal-pdl.ad.e2.rie.gouv.fr/indicateurs_territoriaux_dev/