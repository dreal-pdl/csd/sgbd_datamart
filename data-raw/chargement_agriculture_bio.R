
# chargement_agriculture_bio

library(magrittr)


# chargement des données 2019 et 2020 (archive) ------- 

# choix du type de chargement pour les millésimes 2019 et 2020
# site de l'Agence Bio / chiffres / aller plus loin : https://www.agencebio.org/vos-outils/les-chiffres-cles/#-Allerplusloin
# donne accès à :
# données départementales 2011-2020 : https://www.data.gouv.fr/fr/datasets/surfaces-cheptels-et-nombre-doperateur-par-departement/
# données communales 2019-2020 : https://www.data.gouv.fr/fr/datasets/surfaces-cheptels-et-nombre-doperateurs-bio-a-la-commune/
# API professionnels bio - api.gouv.fr orientées opérateurs : https://api.gouv.fr/les-api/api-professionnels-bio

# Le présent script repose sur l'exploitation des données communales 2019-2020
# 1 fichier xlsx comprenant 5 onglets : Infos, Exemples, Opérateurs, Surfaces, Cheptels (export_1)
# 1 fichier csv comprenant 1 seul onglet : donnees-communales-certifiees-2020-agencebio-surfaces (export_2) : celui que nous retenons

# url_export_1 <- "https://static.data.gouv.fr/resources/surfaces-et-cheptels-certifies-bio-par-commune/20211206-150430/donnees-communales-certifiees-2019-2020-agencebio.xlsx"
# download.file(url = url_export_1, destfile = "extdata/donnees-communales-certifiees-2019-2020-agencebio.xlsx", method = "curl") 
# export_1 <- read_excel(path = "extdata/donnees-communales-certifiees-2019-2020-agencebio.xlsx", 
#                        sheet = "Surfaces")
# # names(export_1)
# # [1] "annee"              "codecommune"        "libellecommune"     "com_code_postal"    "canton"             "arrondissement"    
# # [7] "codeepci"           "libelleepci"        "codedepartement"    "libelledepartement" "coderegion"         "libelleregion"     
# # [13] "code_groupe"        "nb_exp"             "surfab"             "surfc1"             "surfc2"             "surfc3"            
# # [19] "surfc123"           "surfbio"

# url_export_2 <- "https://static.data.gouv.fr/resources/surfaces-et-cheptels-certifies-bio-par-commune/20211206-142728/donnees-communales-certifiees-2020-agencebio-surfaces.csv"
# export_2 <- read.csv2(file = url_export_2, sep = ";", encoding = "UTF-8")
# names(export_2)
# [1] "annee"              "codecommune"        "libellecommune"     "com_code_postal"    "canton"             "arrondissement"     "codeepci"           "libelleepci"        "codedepartement"   
# [10] "libelledepartement" "coderegion"         "libelleregion"      "code_groupe"        "nb_exp"             "surfab"             "surfc1"             "surfc2"             "surfc3"            
# [19] "surfc123"           "surfbio"  

# agri_bio_2019_2020 <- export_2 %>%
#   select(annee, codecommune, code_groupe, nb_exp, surfab:surfbio) %>%
#   filter(codecommune != "00000") %>%
#   rename(depcom = codecommune) %>%
#   mutate(date = dmy(paste("31-12", annee, sep = "-"))) %>%
#   select(-annee) %>%
#   select(-code_groupe) %>% 
#   group_by(depcom, date) %>% 
#   mutate(nb_exp = sum(nb_exp),
#          surfab = sum(surfab),
#          surfc1 = sum(surfc1),
#          surfc2 = sum(surfc2), 
#          surfc3 = sum(surfc3),
#          surfc123 = sum(surfc123), 
#          surfbio = sum(surfbio)) %>% 
#   unique() %>% 
#   ungroup() %>%
#   rename(nb_exploitation_engagee_bio = nb_exp,
#          surface_terme_conversion_bio = surfab,
#          surface_conversion_1e_annee_bio = surfc1,
#          surface_conversion_2e_annee_bio = surfc2,
#          surface_conversion_3e_annee_bio = surfc3,
#          surface_totale_conversion_bio = surfc123,
#          surface_totale_engagee_bio = surfbio) %>% 
#   mutate_if(is.character,as.factor)

# Au 12/05/2023, l'url n'est plus active et donne un message d'erreur. 
# Il faut maintenant aller sur la page "Parcelles en Agriculture Biologique (AB) déclarées à la PAC" du site data.gouv.fr
# (https://www.data.gouv.fr/fr/datasets/parcelles-en-agriculture-biologique-ab-declarees-a-la-pac/) 
# pour charger des fichiers shape ou geojson zippés qui donnent des informations par parcelles sur le type de culture
# par sélection de la région pour les années 2019 et 2020 uniquement.
# Les nouveaux champs sont complètement différents de ceux issus de l'url précédente.
# De ce fait, on ne fera pas l'économie d'une réflexion préalable à la construction ou non 
# d'un indicateur pérenne pour l'agriculture biologique.


# chargement des données 2019 et 2022 ------- 
# Au 05/02/2024, L'adresse https://www.agencebio.org/vos-outils/les-chiffres-cles/#-Allerplusloin 
# conduit à une page du site de l'AgenceBio qui comporte maintenant une partie intitulée "Les données ouvertes". 
# Cette dernière met à disposition plusieurs liens dont "Les surfaces, cheptels et nombre d’opérateurs à la commune" 
# (https://www.data.gouv.fr/fr/datasets/surfaces-cheptels-et-nombre-doperateurs-bio-a-la-commune/).
# Le lien de téléchargement du fichier surfaces_cheptels_operateurs_communes_2019_2022_agencebio.xlsx 
# est https://www.data.gouv.fr/fr/datasets/r/9db0425a-7de0-4197-88aa-b248be86e7f4
# le fichier .xlsx mentionne la liste officielle des communes et EPCI de l'INSEE au 1er janvier 2023 : COG2022 ou COG2023 ?

url_export <- "https://www.data.gouv.fr/fr/datasets/r/9db0425a-7de0-4197-88aa-b248be86e7f4"
utils::download.file(
  url = url_export, destfile = "extdata/surfaces_cheptels_operateurs_communes_2019_2022_agencebio.xlsx", 
  method = "curl",
  extra = "-L"
  )
export <- readxl::read_excel(path = "extdata/surfaces_cheptels_operateurs_communes_2019_2022_agencebio.xlsx", sheet = "Surfaces")

# names(export)
# [1] "annee"              "codecommune"        "libellecommune"     "com_code_postal"    "canton"             "arrondissement"    
# [7] "codeepci"           "libelleepci"        "codedepartement"    "libelledepartement" "coderegion"         "libelleregion"     
# [13] "code_groupe"        "nb_exp"             "surfab"             "surfc1"             "surfc2"             "surfc3"            
# [19] "surfc123"           "surfbio"   

agri_bio_2019_2022 <- export %>%
  dplyr::select(annee, codecommune, code_groupe, nb_exp, surfab:surfbio) %>%
  dplyr::mutate(nb_exp = as.numeric(nb_exp)) %>% 
  dplyr::filter(codecommune != "00000") %>%
  dplyr::rename(depcom = codecommune) %>%
  dplyr::mutate(date = lubridate::dmy(paste("31-12", annee, sep = "-"))) %>%
  dplyr::select(-annee) %>%
  dplyr::select(-code_groupe) %>% 
  dplyr::group_by(depcom, date) %>% 
  dplyr::mutate(
    nb_exp = sum(nb_exp),
    surfab = sum(surfab),
    surfc1 = sum(surfc1),
    surfc2 = sum(surfc2), 
    surfc3 = sum(surfc3),
    surfc123 = sum(surfc123), 
    surfbio = sum(surfbio)
    ) %>% 
  unique() %>% 
  dplyr::ungroup() %>%
  dplyr::rename(
    nb_exploitation_engagee_bio = nb_exp,
    surface_terme_conversion_bio = surfab,
    surface_conversion_1e_annee_bio = surfc1,
    surface_conversion_2e_annee_bio = surfc2,
    surface_conversion_3e_annee_bio = surfc3,
    surface_totale_conversion_bio = surfc123,
    surface_totale_engagee_bio = surfbio
    ) %>% 
  dplyr::mutate_if(is.character, as.factor) %>% 
  dplyr::select(depcom, date, tidyselect::everything()) %>% 
  tidyr::gather(key = "variable", value = "valeur", nb_exploitation_engagee_bio:surface_totale_engagee_bio) %>% 
  tidyr::complete(depcom, date, variable, fill = list(valeur = 0), explicit = FALSE) %>% 
  tidyr::pivot_wider(names_from = variable, values_from = valeur) # 80324 obs
# liste_com_agri_bio <- agri_bio_2019_2022 %>% dplyr::select(depcom) %>% unique() # 20081 obs


# complétude de la liste des communes -------
liste_COG_2023 <- COGiter::communes %>%
  dplyr::select(depcom = DEPCOM) %>%
  dplyr::pull() %>% 
  as.character() # 34945 obs

agri_bio_2019_2022_2 <- agri_bio_2019_2022 %>% 
  dplyr::mutate(depcom = forcats::fct_expand(depcom, liste_COG_2023)) %>% 
  tidyr::complete(depcom, date, fill = list(
    nb_exploitation_engagee_bio = 0,
    surface_terme_conversion_bio = 0,
    surface_conversion_1e_annee_bio = 0,
    surface_conversion_2e_annee_bio = 0,
    surface_conversion_3e_annee_bio = 0,
    surface_totale_conversion_bio = 0,
    surface_totale_engagee_bio = 0
    )
  )


# export vers le SGBD -----------
source("R/poster_documenter_data.R", encoding = "UTF-8")
poster_documenter_it(
  df = agri_bio_2019_2022_2 ,
  nom_table_sgbd = "source_agriculture_bio", 
  comm_source_en_plus = ""
)


# Descriptif de la source -------------

# Le descriptif de la source dans l'onglet sources du ggoglesheet du projet des indicateurs territoriaux était la suivant :
# Ces données de surfaces sont issues des contrôles annuels disponibles depuis 2011 que les organismes certificateurs agréés,
# réalisent dans les fermes et les entreprises de transformation, distribution engagées en bio. 
# Comme une commune sur deux est concernée par le secret statistique des données (disposer d’au moins 3 exploitations), 
# les données communales disponibles depuis 2019 ne sont pas prises en compte. 
# Au niveau des EPCI, seules 2 sont couvertes par le secret statistique.

# Nouveau descriptif : 
# Ces données de surfaces sont issues des contrôles annuels disponibles depuis 2011 que les organismes certificateurs agréés,
# réalisent dans les fermes et les entreprises de transformation, distribution engagées en bio.
# Depuis la mise à jour du 6 décembre 2021, les données communales brutes sont disponibles. 
# Deux sortes de table cohabitent donc sur le sgbd/datamart/portrait_territoires : 
# - celles issue de chargment des données communales brutes à partir du millésime 2019 ;
# - celle issue des données à l'EPCI', au département ou à la région du millésime 2011 à 2019 (specifique_agriculture_bio)
#   pour laquelle le secret statistique était invoqué.

# Descriptif définitif
# Ces données de surfaces sont issues des contrôles annuels disponibles depuis 2011 que les organismes certificateurs agréés,
# réalisent dans les fermes et les entreprises de transformation, distribution engagées en bio.
# Depuis la mise à jour du 6 décembre 2021, les données communales brutes sont disponibles. 
# Deux sortes de table cohabitent donc sur le sgbd/datamart/portrait_territoires : 
# - celles issue de chargment des données communales brutes à partir du millésime 2019 ;
# - celle issue des données à l'EPCI', au département ou à la région du millésime 2011 à 2019 (zz_agriculture_bio)
#   pour laquelle le secret statistique était invoqué au niveau communal et qui sont archivées de ce fait.





