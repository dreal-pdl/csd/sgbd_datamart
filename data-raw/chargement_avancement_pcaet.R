
# chargement_avancement_pcaet

# Indicateurs proposés CRTE : Etat d'avancement des PCAET
# on récupère les données de la carte MECC produite par DSIT sur le SGBD


# librairies -----
library(dplyr)
library(tidyr)
library(forcats)
library(stringr)
library(COGiter)
library(lubridate)
library(tricky)
library(datalibaba)
library(googlesheets4)


rm(list = ls())


# chargement des données ----------

# lecture du tableur de gestion MECC, plus complet que la table sgbd
pcaet_tab1 <- readxl::read_xls(path = "T:/geomatique/PROJETS/MECC/PCAET/DONNEES_CLIENT/suivi_pcaet.xls",
                               sheet = 2, col_names = TRUE)

# on récupère aussi le 1er onglet, pour disposer de la valeur des champs calculés dans le 2d onglet (Type,...)
pcaet_tab2 <- readxl::read_xls(path = "T:/geomatique/PROJETS/MECC/PCAET/DONNEES_CLIENT/suivi_pcaet.xls",
                               sheet = 1, col_names = TRUE) %>% 
  # on recalcule les champs calculés avec une formule qui ressortent à 0 dans R
  mutate(id_pcaet=paste0("PCAET-", dpt, "-", chrono))

lib_avancmt <- readxl::read_xls(path = "T:/geomatique/PROJETS/MECC/PCAET/DONNEES_CLIENT/suivi_pcaet.xls",
                                sheet = "Avancement", col_names = TRUE)
lib_avancmt2 <- lib_avancmt %>% 
  bind_rows(data.frame(id_avancement = 0, avancement = "pas de pcaet")) %>% 
  mutate(avancement = gsub(" \\(consultation, instruction en cours\\)|de PCAET |^PCAET ", "", avancement) %>% 
           tolower %>% 
           gsub("à effectuer ou à mettre à jour", "à lancer ou relancer",.) %>% 
           gsub("en cours d’élaboration", "élaboration en cours", .) %>% 
           gsub("avec mention exemplaire", "approuvé et exemplaire", .) %>% 
           gsub("territoire non obligé", "pas de pcaet", .) %>% 
           as.factor() %>% fct_inorder()) %>% 
  select(contains("avancement")) 


# calcul ---------
avcmt_pcaet <- pcaet_tab1 %>% 
  set_standard_names %>% 
  select(id_ter = siren_epci, id_pcaet = pcaet, oblige_pcaet = oblige_pceat, id_avancmt) %>% 
  left_join(pcaet_tab2 %>% select(id_pcaet, nom_pcaet, echelle_pcaet = scot_epci, tepcv),
            by = "id_pcaet") %>% 
  # typage des champs et redressement
  mutate(id_ter = as.character(id_ter),
         nom_pcaet = as.factor(nom_pcaet),
         opposabilite_pcaet = id_avancmt %in% c(5:6),
         id_pcaet = na_if(id_pcaet, "PCAET--non"),
         oblige_pcaet = ( oblige_pcaet == "oui" ),
         tepcv = ( tepcv == "oui" ),
         echelle_pcaet = tolower(echelle_pcaet) %>% as.factor) %>% 
  left_join(lib_avancmt2, by = c("id_avancmt" = "id_avancement")) %>% 
  rename(avancmt_pcaet = avancement) %>% 
  select(-id_avancmt)

# ajout des communes et vérif complétude epci de la région
epci_reg <- filter(epci, grepl("52", REGIONS_DE_L_EPCI)) %>% 
  select(CodeZone = EPCI, Zone = NOM_EPCI) 

# a-ton des epci hors région ou qui n'existent plus ? (0 ligne attendu)
filter(avcmt_pcaet, !(id_ter %in% pull(epci_reg, CodeZone)), echelle_pcaet != "commune"|is.na(echelle_pcaet)) 

# Manque-t-il des EPCI dans la table ? (0 attendu)
setdiff(epci_reg$CodeZone, avcmt_pcaet$id_ter)

# création de la table communale
com_reg <- filter(communes, grepl("52", REGIONS_DE_L_EPCI)) %>% 
  select(CodeZone = DEPCOM, Zone = NOM_DEPCOM, EPCI) %>% 
  filter(!grepl("ZZZ", EPCI))

avcmt_pcaet_com <- filter(avcmt_pcaet, echelle_pcaet != "commune"|is.na(echelle_pcaet)) %>% 
  full_join(com_reg, by = c("id_ter" = "EPCI")) %>% 
  bind_rows(filter(avcmt_pcaet, echelle_pcaet == "commune") %>% 
              left_join(communes %>% select(DEPCOM, Zone = NOM_DEPCOM) %>% mutate(CodeZone = DEPCOM), 
                        by = c("id_ter" = "DEPCOM"))) %>% 
  mutate(TypeZone = "Communes") %>% 
  select(contains("Zone"), oblige_pcaet, contains("_pcaet"), tepcv, -id_ter)

avcmt_pcaet2 <- avcmt_pcaet %>% 
  filter(echelle_pcaet != "commune"|is.na(echelle_pcaet)) %>% 
  mutate(TypeZone = "Epci") %>% 
  rename(CodeZone = id_ter) %>% 
  full_join(epci_reg, by = c("CodeZone")) %>% 
  bind_rows(avcmt_pcaet_com, .) %>% 
  mutate(across(where(is.factor), ~ as.factor(.x) %>% fct_drop),
         date = today())


# versement dans le sgbd/datamart.portrait_territoires + documentation -------------

# doublons - TRUE attendu
avcmt_pcaet2 %>% 
  dplyr::count(CodeZone, TypeZone, date) %>% 
  dplyr::filter(n>1) %>% 
  nrow() == 0

datalibaba::poster_data(data = avcmt_pcaet2,
                        db = "datamart",
                        schema = "portrait_territoires", 
                        table = "ref_avancement_pcaet",
                        post_row_name = FALSE, 
                        overwrite = TRUE,
                        droits_schema = TRUE,
                        pk = c("CodeZone", "TypeZone", "date"), # déclaration d'une clé primaire sur la table postée : on ne doit pas avoir deux lignes avec à la fois le même code commune et la meme date
                        user = "does") 
var <- dplyr::setdiff(names(avcmt_pcaet2), c("CodeZone", "TypeZone", "Zone", "date"))

## récupération du nom du présent script source pour filtrer ensuite le référentiel des indicateurs
nom_script_sce <- rstudioapi::getActiveDocumentContext()$path %>% # utilisation de rstudioapi pour récupérer le nom du présent script 
  basename() %>% # on enlève le chemin d'accès pour ne garder que le nom du fichier
  gsub(pattern = ".R$", "", .) # on enlève l'extension '.R'

## authentification google sheet grâce au .Renviron
googlesheets4::gs4_auth_configure(api_key = Sys.getenv("google_api_key"))
googlesheets4::gs4_deauth()

## chargement du référentiel indicateurs google sheet
metadata_indicateur <- googlesheets4::read_sheet("https://docs.google.com/spreadsheets/d/1n-dhtrJM3JwFVz5WSEGOQzQ8A0G7VT_VcxDe5gh6zSo/edit#gid=60292277",
                                                 sheet = "indicateurs") %>%
  # on ne garde que les variables concernées par le présent script de chargement
  dplyr::filter(source == nom_script_sce) %>% 
  # on ajoute l'unité dans le libellé de la variable
  dplyr::mutate(libelle_variable = paste0(libelle_variable, " (unit\u00e9 : ", unite, ")")) %>% 
  dplyr::select(variable, libelle_variable) %>% 
  # ajout des libellés pour depcom et date
  dplyr::bind_rows(
    tidyr::tribble(
      ~variable, ~libelle_variable,
      "CodeZone", "Code géo du territoire",
      "TypeZone", "Type de territoire (Communes, EPCI, Départements, Régions)",
      "Zone", "Libellé du territoire",
      "date", "Millesime"
    )
  )

## Vérification que la documentation des indicateurs est complète
## variables manquantes 
dplyr::setdiff(var, metadata_indicateur$variable)
## variables en trop
dplyr::setdiff(metadata_indicateur$variable, c("CodeZone", "TypeZone", "Zone", "date", var))

## Envoi des libellés de variable dans le SGBD
datalibaba::post_dico_attr(dico = metadata_indicateur, table = "ref_avancement_pcaet", schema = "portrait_territoires",
                           db = "datamart", user = "does")

## Récupération des métadonnées de la source
nom_sce <- stringr::str_replace(nom_script_sce, "chargement_|ref_|specifique_", "") %>%
  stringr::str_replace("indicateur_", "") %>%
  stringr::str_replace("_cogiter|_cog$", "")

metadata_source <- googlesheets4::read_sheet("https://docs.google.com/spreadsheets/d/1n-dhtrJM3JwFVz5WSEGOQzQ8A0G7VT_VcxDe5gh6zSo/edit#gid=60292277",
                                             sheet = "sources") %>%
  dplyr::filter(source == nom_sce) %>% 
  dplyr::mutate(com_table = paste0(source_lib, " - ", producteur, ".\n", descriptif_sources)) %>% 
  dplyr::pull(com_table) %>% 
  # ajout de complément sur la généalogie
  paste0(".\n", "Visualisation carto sur https://carto.sigloire.fr/1/layers/2b4d5df9-13cd-4384-a7c0-2d2d513b800d.map")


## commentaires de la table

datalibaba::commenter_table(comment = metadata_source,
                            db = "datamart",
                            schema = "portrait_territoires",
                            table = "ref_avancement_pcaet", 
                            user = "does")



rm(list=ls())

