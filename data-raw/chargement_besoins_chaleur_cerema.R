
# chargement_besoins_chaleur_cerema


# librairies -------
library(tidyverse)
library(sf)
library(datalibaba)
library(googlesheets4)

rm(list=ls())


# chargement data -------
# Url à retrouver : https://reseaux-chaleur.cerema.fr/donnees-et-cartographies/carte-nationale-des-besoins-chaleur-telechargement-des-donnees
download.file(# url = "http://reseaux-chaleur.cerema.fr/telechargement/4683/ch_com.zip",
              destfile = "extdata/chaleur_cerema_model_com_2014.zip")
unzip("extdata/chaleur_cerema_model_com_2014.zip", overwrite = TRUE, exdir = "extdata")


# calcul ----------
besoins_chaleur_cerema <- st_read("extdata/chaleur_CEREMA/ch_com.TAB") %>% 
  st_drop_geometry() %>% 
  select(depcom=INSEE_comm, valeur=c_ch_tot) %>%
  mutate(date=as.Date("2008-12-31"), variable=factor("gwh_chaleur_consommes", "gwh_chaleur_consommes"), 
         valeur=(valeur/1000000) %>% round(2) ) %>% # conversion kWh en GWh
  complete(depcom,date,variable,fill = list(valeur =0)) %>% 
  pivot_wider(names_from = variable,values_from = valeur)


# versement dans le sgbd/datamart.portrait_territoires -------------
poster_data(data = besoins_chaleur_cerema,
            db = "datamart",
            schema = "portrait_territoires", 
            table = "source_besoins_chaleur_cerema",
            post_row_name = FALSE, 
            overwrite = TRUE,
            droits_schema = TRUE,
            pk = c("depcom", "date"), # déclaration d'une clé primaire sur la table postée : on ne doit pas avoir deux lignes avec à la fois le même code commune et la meme date
            user = "does")

# METADONNEES------------------------------------

## On récupère la liste des variables qui sont à documenter dans le tableur google sheet à partir du jeu de données posté
var <- setdiff(names(besoins_chaleur_cerema), c("depcom", "date"))

## récupération du nom du présent script source pour filtrer ensuite le référentiel des indicateurs
nom_script_sce <- rstudioapi::getActiveDocumentContext()$path %>% # utilisation de rstudioapi pour récupérer le nom du présent script 
  basename() %>% # on enlève le chemin d'accès pour ne garder que le nom du fichier
  gsub(pattern = ".R$", "", .) # on enlève l'extension '.R'

## authentification google sheet grâce au .Renviron
gs4_auth_configure(api_key = Sys.getenv("google_api_key"))
gs4_deauth()

## chargement du référentiel indicateurs google sheet
metadata_indicateur <- read_sheet("https://docs.google.com/spreadsheets/d/1n-dhtrJM3JwFVz5WSEGOQzQ8A0G7VT_VcxDe5gh6zSo/edit#gid=60292277",
                                  sheet = "indicateurs") %>%
  # on ne garde que les variables concernées par le présent script de chargement
  filter(source == nom_script_sce) %>% 
  # on ajoute l'unité dans le libellé de la variable
  mutate(libelle_variable = paste0(libelle_variable, " (unit\u00e9 : ", unite, ")")) %>% 
  select(variable, libelle_variable) %>% 
  # ajout des libellés pour depcom et date
  bind_rows(
    tribble(
      ~variable, ~libelle_variable,
      "depcom", "Code INSEE de la commune",
      "date", "Millesime"
    )
  )

## Vérification que la documentation des indicateurs est complète
all(var %in% metadata_indicateur$variable) # doit renvoyer TRUE

## Envoi des libellés de variable dans le SGBD
post_dico_attr(dico = metadata_indicateur, table = "source_besoins_chaleur_cerema", schema = "portrait_territoires",
               db = "datamart", user = "does")

## Récupération des métadonnées de la source
nom_sce <- str_replace(nom_script_sce, "chargement_|ref_|specifique_", "") %>%
  str_replace("indicateur_", "") %>%
  str_replace("_cogiter|_cog$", "")

metadata_source <- read_sheet("https://docs.google.com/spreadsheets/d/1n-dhtrJM3JwFVz5WSEGOQzQ8A0G7VT_VcxDe5gh6zSo/edit#gid=60292277",
                              sheet = "sources") %>%
  filter(source == nom_sce) %>% 
  mutate(com_table = paste0(source_lib, " - ", producteur, ".\n", descriptif_sources)) %>% 
  pull(com_table)%>% 
  # ajout de complement sur la généalogie
  paste0(".\n", "")


## commentaires de la table

commenter_table(comment = metadata_source,
                db = "datamart",
                schema = "portrait_territoires",
                table = "source_besoins_chaleur_cerema", 
                user = "does")



