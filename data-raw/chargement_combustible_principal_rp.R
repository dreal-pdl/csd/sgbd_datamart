
# chargement_combustible_principal_rp

# données disponibles sur :
# 2017 --> https://www.insee.fr/fr/statistiques/4515537?sommaire=4516107
# Logements et résidences principales en 2017, Recensement de la population
# PRINC30M – Résidences principales par type de logement, statut d'occupation et combustible principal (France métropolitaine)
# https://www.insee.fr/fr/statistiques/fichier/4515537/BTT_TD_PRINC30M_2017.zip
# paru le 09/12/2020

# 2018 --> https://www.insee.fr/fr/statistiques/5395863?sommaire=5395912
# https://www.insee.fr/fr/statistiques/fichier/5395863/BTT_TD_PRINC30M_2018.zip
# paru le 30/06/2021

# 2019 --> https://www.insee.fr/fr/statistiques/6454260?sommaire=6454268
# https://www.insee.fr/fr/statistiques/fichier/6454260/BTT_TD_PRINC30M_2019.zip
# paru le 27/06/2022

# 2020 --> https://www.insee.fr/fr/statistiques/7631446?sommaire=7631713
# https://www.insee.fr/fr/statistiques/fichier/7631446/TD_PRINC30M_2020_csv.zip
# paru le 27/06/2023

# 2021 --> https://www.insee.fr/fr/statistiques/8202355?sommaire=8202874
# https://www.insee.fr/fr/statistiques/fichier/8202355/TD_PRINC30M_2021_csv.zip
# paru le 27/06/2024

# librairies ----------
library(tidyverse)
library(lubridate)
library(datalibaba)
# library(googlesheets4)

rm(list = ls())


# chargement data et calcul -------

download.file(url = "https://www.insee.fr/fr/statistiques/fichier/8202355/TD_PRINC30M_2021_csv.zip", 
              destfile = "extdata/BTT_TD_PRINC30M_2021.zip")
unzip("extdata/BTT_TD_PRINC30M_2021.zip", overwrite = TRUE, exdir = "extdata")
BTT_TD_PRINC30M_2021 <- read.csv2("extdata/TD_PRINC30M_2021.csv",
                                  dec='.', 
                                  header = TRUE, 
                                  sep=";", 
                                  stringsAsFactors = FALSE,
                                  encoding = "UTF-8")

date = 2021

combustible_2021 <- BTT_TD_PRINC30M_2021 %>%
  filter(NIVGEO == "COM") %>%
  select (-NIVGEO , -LIBGEO) %>%
  group_by(CODGEO, CMBL, TYPLR) %>%
  summarise(valeur = sum(NB, na.rm=T)) %>%
  ungroup() %>%
  mutate(variable = paste(CMBL,TYPLR)) %>%
  mutate(variable = replace(variable, variable == "1 1", "nb_rp_maisons_combustible_chauffage_urbain"),
         variable = replace(variable, variable == "1 2", "nb_rp_appartements_combustible_chauffage_urbain"),
         variable = replace(variable, variable == "1 3", "nb_rp_autres_combustible_chauffage_urbain"),
         variable = replace(variable, variable == "2 1", "nb_rp_maisons_combustible_gaz_de_ville_ou_de_reseau"),
         variable = replace(variable, variable == "2 2", "nb_rp_appartements_gaz_de_ville_ou_de_reseau"),
         variable = replace(variable, variable == "2 3", "nb_rp_autres_combustible_gaz_de_ville_ou_de_reseau"),
         variable = replace(variable, variable == "3 1", "nb_rp_maisons_combustible_fioul"),
         variable = replace(variable, variable == "3 2", "nb_rp_appartements_combustible_fioul"),
         variable = replace(variable, variable == "3 3", "nb_rp_autres_combustible_fioul"),
         variable = replace(variable, variable == "4 1", "nb_rp_maisons_combustible_electricite"),
         variable = replace(variable, variable == "4 2", "nb_rp_appartements_combustible_electricite"),
         variable = replace(variable, variable == "4 3", "nb_rp_autres_combustible_electricite"),
         variable = replace(variable, variable == "5 1", "nb_rp_maisons_combustible_gaz_bouteille_ou_citerne"),
         variable = replace(variable, variable == "5 2", "nb_rp_appartements_combustible_gaz_bouteille_ou_citerne"),
         variable = replace(variable, variable == "5 3", "nb_rp_autres_combustible_gaz_bouteille_ou_citerne"),
         variable = replace(variable, variable == "6 1", "nb_rp_maisons_combustible_autre"),
         variable = replace(variable, variable == "6 2", "nb_rp_appartements_combustible_autre"),
         variable = replace(variable, variable == "6 3", "nb_rp_autres_combustible_autre")) %>%
  mutate(date = make_date(date, 12, 31)) %>%
  rename(depcom = CODGEO) %>% 
  select (depcom, date, variable, valeur) %>%  
  complete(depcom, date, variable, fill = list(valeur = 0)) %>% 
  pivot_wider(names_from = variable, values_from = valeur)

# combustible <- bind_rows(combustible_2018, combustible_2019, combustible_2020) # rattrapage des millésimes 2018 et 2019

combustible_old <- importer_data(db = "datamart",
                                 schema = "portrait_territoires",
                                 table = "source_combustible_principal_rp") # 2017

combustible <- bind_rows(combustible_old, combustible_2021)


# versement dans le sgbd/datamart.portrait_territoires et metadonnées -------------
source("R/poster_documenter_data.R", encoding = "UTF-8")
poster_documenter_it(df = combustible ,
                     nom_table_sgbd = "source_combustible_principal_rp", 
                     comm_source_en_plus = "")

rm(list = ls())

