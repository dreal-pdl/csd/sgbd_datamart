
# compatibilite_steu_normes_ue


library(magrittr)

rm(list = ls())


# indicateur CRTE

# le portail d'information sur l'assainissement communal permet de charger les données de l'année 2014
# jusqu'à l'année n-2, en l'occurrence 2019

# Au 07/06/2022, les fonctions de téléchargement du script ne fonctionnent plus, car les modalités de téléchargement ont été modifiées
# sur le site https://www.assainissement.developpement-durable.gouv.fr/PortailAC/
# Il faut en passer par une fenêtre de "Téléchargement des données et Outils d'édition de fichiers SANDRE autosurveillance".
# téléchargement à la main, par année

# Au 12/05/2023, comme au 07/06/2022 pour l'année 2021

# Au 07/02/2024, presque idem : https://assainissement.developpement-durable.gouv.fr/pages/data/basededonneesteu.php



# # fonctions d'exportation et de première préparation -----------
# 
# creer_export_eru_2014_2017 <- function(annee){
#   # url_export_eru<-paste0("http://assainissement.developpement-durable.gouv.fr/Export_ERU_",annee,".csv")
#   # export_eru<-download.file(url = url_export_eru,destfile = paste0("extdata/Export_ERU_",annee,".csv") )
#   export_eru<-read.csv2(file = paste0("extdata/Export_ERU_",annee,".csv"),sep = ";",encoding = "UTF-8") %>%
#     select(Nom.de.la.région,
#            Numéro.département,
#            Code.SANDRE.de.l.agglo,
#            Nom.de.l.agglo,
#            Nom.de.la.commune.principale,
#            Code.INSEE.commune.principale,
#            Nombre.de.communes,
#            Tranche.d.obligation,
#            Code.du.STEU,
#            Nom.du.STEU,
#            Code.INSEE.commune.d.implantation,
#            Conformité.globale.performances,
#            Nom.du.milieu.de.rejet,
#            Type.du.milieu.du.rejet) %>%
#     mutate(annee = annee)
# 
#   return(export_eru)
# }
# 
# creer_export_eru_2018_2019 <- function(annee){
#   # url_export_eru<-paste0("http://assainissement.developpement-durable.gouv.fr/Export_ERU_",annee,".csv")
#   # export_eru<-download.file(url = url_export_eru,destfile = paste0("extdata/Export_ERU_",annee,".csv") )
#   export_eru<-read.csv2(file = paste0("extdata/Export_ERU_",annee,".csv"),sep = "|",encoding = "UTF-8") %>%
#     select(Nom.de.la.région,
#            Numéro.département,
#            Code.SANDRE.de.l.agglo,
#            Nom.de.l.agglo,
#            Nom.de.la.commune.principale,
#            Code.INSEE.commune.principale,
#            Nombre.de.communes,
#            Tranche.d.obligation,
#            Code.du.STEU,
#            Nom.du.STEU,
#            Code.INSEE.commune.d.implantation,
#            Conformité.globale.performances,
#            Nom.du.milieu.de.rejet,
#            Type.du.milieu.du.rejet) %>%
#     mutate(annee = annee)
# 
#   return(export_eru)
# }
# 
# export_eru_2014_2017 <- map(c(2014:2017),~creer_export_eru_2014_2017(.x))
# export_eru_2014_2017 <- rbindlist(export_eru_2014_2017, fill = TRUE, idcol = F)
# 
# # export_eru_2018_2019<-map(c(2018:2019),~creer_export_eru_2018_2019(.x))
# # export_eru_2018_2019<-rbindlist(export_eru_2018_2019, fill = TRUE, idcol = F)
# 
# export_eru_2019 <- creer_export_eru_2018_2019(2019)
# 
# export_eru_2014_2017_2019 <- bind_rows(export_eru_2014_2017, export_eru_2019) %>%
#   rename(Code.SANDRE.de.l.agglomération = Code.SANDRE.de.l.agglo,
#          Nom.de.l.agglomération = Nom.de.l.agglo,
#          code_commune = Code.INSEE.commune.principale,
#          # code_commune = Code.INSEE.commune.d.implantation, # implantation du rejet a priori, pas de la station
#          tranche = Tranche.d.obligation,
#          conformite = Conformité.globale.performances) %>%
#   mutate(conformite = ifelse(conformite %in% c("?","N/A"),"Inc",conformite))


# préparation du fichier importé du portail assainissement collectif --------
prepa_eru <- function(export_eru, annee){
  export_eru_prepa <- export_eru %>% 
    dplyr::select(
      Nom.de.la.région,
      Numéro.département,
      Code.SANDRE.de.l.agglomération,
      Nom.de.l.agglomération,
      Nom.de.la.commune.principale,
      Code.INSEE.commune.principale,
      X.Nombre.de.communes,
      Tranche.obligation,
      Code.du.STEU,
      Nom.du.STEU,
      Code.INSEE.commune.implantation,
      Conformité.globale.performances,
      Nom.du.milieu.de.rejet,
      Type.du.milieu.du.rejet
      ) %>%
    dplyr::mutate(annee = annee) %>%
    dplyr::rename(
      code_commune = Code.INSEE.commune.principale,
      Nombre.de.communes = X.Nombre.de.communes,
      tranche = Tranche.obligation,
      conformite = Conformité.globale.performances,
      Code.INSEE.commune.d.implantation = Code.INSEE.commune.implantation
      ) %>%
    dplyr::mutate(conformite = ifelse(conformite %in% c("?","N/A"), "Inc", conformite))
}


# # récupération de 2018 ----------
# export_eru <- read.csv2(file = "extdata/db_export_20182023-08-10.csv",sep = "|",encoding = "UTF-8")
# export_eru_2018 <- prepa_eru(export_eru, 2018)
# 
# 
# # actualisation pour 2020 -------
# export_eru <- read.csv2(file = "extdata/db_export_20202022-06-07.csv",sep = "|",encoding = "UTF-8")
# export_eru_2020 <- prepa_eru(export_eru, 2020)
# 
# 
# # actualisation 2021 ----------
# export_eru <- read.csv2(file = "extdata/db_export_20212023-05-12.csv",sep = "|",encoding = "UTF-8")
# export_eru_2021 <- prepa_eru(export_eru, 2021)


# calcul de l'indicateur ---------
calcul_indicateur_eru <- function(export_eru){
  eru1 <- export_eru %>%
    dplyr::group_by(code_commune, tranche, conformite, annee) %>% 
    dplyr::summarise(valeur = dplyr::n()) %>%
    dplyr::ungroup() %>% 
    dplyr::mutate(
      variable = dplyr::case_when(
        tranche=="Taille < 200 EH" & conformite == "Oui"~ "nbre_steu_conforme_inf_200_eh",
        tranche=="Taille < 200 EH" & conformite == "Non"~ "nbre_steu_non_conforme_inf_200_eh",
        tranche=="Taille < 200 EH" & conformite == "Inc"~ "nbre_steu_conformite_inconnues_inf_200_eh",
        
        tranche=="[ 200 ; 2 000 [ EH" & conformite == "Oui"~ "nbre_steu_conforme_sup_200_inf_2000_eh",
        tranche=="[ 200 ; 2 000 [ EH" & conformite == "Non"~ "nbre_steu_non_conforme_sup_200_inf_2000_eh",
        tranche=="[ 200 ; 2 000 [ EH" & conformite == "Inc"~ "nbre_steu_conformite_inconnue_sup_200_inf_2000_eh",
        
        tranche=="[ 2 000 ; 10 000 [ EH" & conformite == "Oui"~ "nbre_steu_conforme_sup_2000_inf_10000_eh",
        tranche=="[ 2 000 ; 10 000 [ EH" & conformite == "Non"~ "nbre_steu_non_conforme_sup_2000_inf_10000_eh",
        tranche=="[ 2 000 ; 10 000 [ EH" & conformite == "Inc"~ "nbre_steu_conformite_inconnue_sup_2000_inf_10000_eh",
        
        tranche=="[ 10 000 ; 100 000 [ E" & conformite == "Oui"~ "nbre_steu_conforme_sup_10000_inf_100000_eh",
        tranche=="[ 10 000 ; 100 000 [ E" & conformite == "Non"~ "nbre_steu_non_conforme_sup_10000_inf_100000_eh",
        tranche=="[ 10 000 ; 100 000 [ E" & conformite == "Inc"~ "nbre_steu_conformite_inconnue_sup_10000_inf_100000_eh",
        
        tranche=="[ 100 000 ; ... [ EH" & conformite == "Oui"~ "nbre_steu_conforme_sup_100000_eh",
        tranche=="[ 100 000 ; ... [ EH" & conformite == "Non"~ "nbre_steu_non_conforme_sup_100000_eh",
        tranche=="[ 100 000 ; ... [ EH" & conformite == "Inc"~ "nbre_steu_conformite_inconnue_sup_100000_eh"
        )
      ) %>% 
    dplyr::select(-c(tranche,conformite)) %>% 
    dplyr::rename(depcom = code_commune)
  
  eru2 <- export_eru %>%
    dplyr::group_by(code_commune, conformite, annee) %>%
    dplyr::summarise(valeur = dplyr::n()) %>% 
    dplyr::ungroup() %>% 
    dplyr::mutate(
      variable = dplyr::case_when(
        conformite=="Oui"~ "nbre_steu_conforme",
        conformite=="Non"~ "nbre_steu_non_conforme",
        conformite=="Inc"~ "nbre_steu_conformite_inconnue"
        )
      ) %>% 
    dplyr::select(-conformite) %>% 
    dplyr::rename(depcom = code_commune)
  
  eru3 <- export_eru %>%
    dplyr::group_by(code_commune, annee) %>%
    dplyr::summarise(valeur = dplyr::n()) %>% 
    dplyr::ungroup() %>% 
    dplyr::mutate(variable = "nbre_steu") %>% 
    dplyr::rename(depcom = code_commune)
  
  eru4<-export_eru %>% 
    dplyr::group_by(code_commune, tranche,annee) %>% 
    dplyr::summarise(valeur = dplyr::n()) %>%
    dplyr::ungroup() %>% 
    dplyr::mutate(
      variable = dplyr::case_when(
        tranche == "Taille < 200 EH"~ "nbre_steu_inf_200_eh",
        tranche == "[ 200 ; 2 000 [ EH"~ "nbre_steu_sup_200_inf_2000_eh",
        tranche == "[ 2 000 ; 10 000 [ EH"~ "nbre_steu_sup_2000_inf_10000_eh",
        tranche == "[ 10 000 ; 100 000 [ E"~ "nbre_steu_sup_10000_inf_100000_eh",
        tranche == "[ 100 000 ; ... [ EH"~ "nbre_steu_sup_100000_eh"
        )
      ) %>% 
    dplyr::select(-tranche) %>% 
    dplyr::rename(depcom = code_commune)
  
  indicateur_steu_normes_ue <- dplyr::bind_rows(eru1, eru2, eru3, eru4) %>%
    dplyr::mutate(date = as.Date(paste0(annee,"-12-31"))) %>% 
    dplyr::select(-annee) %>% 
    tidyr::complete(depcom, date, variable, fill = list(valeur = 0), explicit = FALSE) %>%
    dplyr::filter(!is.na(variable)) %>% 
    tidyr::pivot_wider(names_from = variable, values_from = valeur)
}


# # calcul de l'indicateur pour les années 2014 à 2021 ----------
# export_eru_2014_a_2021 <- bind_rows(export_eru_2014_2017_2019,
#                                     export_eru_2018,
#                                     export_eru_2020,
#                                     export_eru_2021)
# indicateur_steu_normes_ue <- calcul_indicateur_eru(export_eru_2014_a_2021)


# pour les millesimes à venir -----------
# téléchargement à la main du fichier .csv annuel et versement dans le répertoire extdata du projet
# puis :
# export_eru <- read.csv2(file = "extdata/db_export_.csv",sep = "|",encoding = "UTF-8")
# export_eru_annee <- prepa_eru(export_eru, annee)
# indicateur_steu_normes_ue <- calcul_indicateur_eru(export_eru_annee)
# chargement de la table source_compatibilite_steu_normes_ue du sgbd/damart/portrait_territoires
# source_compatibilite_steu_normes_ue <- datalibaba::importer_data(
#   db = "datamart",
#   schema = "portrait_territoires",
#   table = "source_compatibilite_steu_normes_ue"
#   )
# ajout de l'indicateur à l'ancienne table
# indicateur_steu_normes_ue <- dplyr::bind_rows(
#   source_compatibilite_steu_normes_ue,
#   indicateur_steu_normes_ue
#   )


# actualisation 2022 ------
export_eru <- utils::read.csv2(file = "extdata/export-as-telechargement_2022.csv",sep = "|",encoding = "UTF-8")
export_eru_2022 <- prepa_eru(export_eru, 2022)
indicateur_steu_normes_ue <- calcul_indicateur_eru(export_eru_2022)
source_compatibilite_steu_normes_ue <- datalibaba::importer_data(
  db = "datamart",
  schema = "portrait_territoires",
  table = "source_compatibilite_steu_normes_ue"
  )
indicateur_steu_normes_ue_2 <- dplyr::bind_rows(
  source_compatibilite_steu_normes_ue,
  indicateur_steu_normes_ue
  )


# complétude de la liste des communes -------
liste_COG_2023 <- COGiter::communes %>%
  dplyr::select(depcom = DEPCOM) %>%
  dplyr::pull() %>% 
  as.character() # 34945 obs

indicateur_steu_normes_ue_3 <- indicateur_steu_normes_ue_2 %>% 
  dplyr::mutate(depcom = forcats::fct_expand(depcom, liste_COG_2023)) %>% 
  tidyr::complete(depcom, date, fill = list(
    nbre_steu = 0,
    nbre_steu_conforme = 0,
    nbre_steu_conforme_inf_200_eh = 0,
    nbre_steu_conforme_sup_10000_inf_100000_eh = 0,
    nbre_steu_conforme_sup_100000_eh = 0,
    nbre_steu_conforme_sup_200_inf_2000_eh = 0,
    nbre_steu_conforme_sup_2000_inf_10000_eh = 0,
    nbre_steu_conformite_inconnue = 0,
    nbre_steu_conformite_inconnue_sup_10000_inf_100000_eh = 0,
    nbre_steu_conformite_inconnue_sup_100000_eh = 0,
    nbre_steu_conformite_inconnue_sup_200_inf_2000_eh = 0,
    nbre_steu_conformite_inconnue_sup_2000_inf_10000_eh = 0,
    nbre_steu_conformite_inconnues_inf_200_eh = 0,
    nbre_steu_inf_200_eh = 0,
    nbre_steu_non_conforme = 0,
    nbre_steu_non_conforme_inf_200_eh = 0,
    nbre_steu_non_conforme_sup_10000_inf_100000_eh = 0,
    nbre_steu_non_conforme_sup_100000_eh = 0,
    nbre_steu_non_conforme_sup_200_inf_2000_eh = 0,
    nbre_steu_non_conforme_sup_2000_inf_10000_eh = 0,
    nbre_steu_sup_10000_inf_100000_eh = 0,
    nbre_steu_sup_100000_eh = 0,
    nbre_steu_sup_200_inf_2000_eh = 0,
    nbre_steu_sup_2000_inf_10000_eh = 0
    )
  )


# versement dans sgbd/datamart.portrait_territoires et metadonnées -------------
source("R/poster_documenter_data.R", encoding = "UTF-8")
poster_documenter_it(
  df = indicateur_steu_normes_ue_3 ,
  nom_table_sgbd = "source_compatibilite_steu_normes_ue",
  comm_source_en_plus = ""
  )
