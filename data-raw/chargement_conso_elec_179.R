
# chargement_conso_elec

# consommation electrique art 179 ltecv fournies par le SDES

# on récupère les données préalablement nettoyées dans le cadre du POC ENR TEO dans le SGBD
# script de nettoyage : https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/enr_reseaux_teo/-/tree/dev/collecte/conso_elec
# initialement fournies par https://www.statistiques.developpement-durable.gouv.fr/donnees-locales-de-consommation-denergie 


# librairies ---------
library(dplyr)
library(tidyr)
library(forcats)
library(stringr)
library(readr)
library(purrr)
library(COGiter)
library(lubridate)
library(datalibaba)
library(googlesheets4)

rm(list=ls())


# paramètres -----------
mill <- 2022
sgbd <- TRUE

if(sgbd) {
  adr_enr_teo <- ""
} else {
  adr_enr_teo <- "C:/Users/juliette.engelaere/Documents/Travail/R_local/enr_reseaux_teo/" 
  # ne fonctionne que sur le poste de JEL si sgbd <- FALSE
}


# lecture des données versées dans le sgbd dans le cadre du POC ENR TEO --------
if (sgbd) {

  conso_elec_179_0 <- importer_data(table = paste0("conso_elec_2008_", mill, "_frce_entiere"), 
                                    schema = "mecc_electricite", db = "production")
  
} else {
  load(paste0(adr_enr_teo, "collecte/sgbd/conso_elec_2008-", mill, "_Frce_entiere.RData"))
  conso_elec_179_0 <- compil_tot %>% 
    rename_all(tolower) %>% 
    mutate(across(where(is.factor), as.character))
  load(paste0(adr_enr_teo, "collecte/sgbd/spec_conso.RData"))
}


# début dataprep -------------
conso_elec_179_1 <- conso_elec_179_0 %>%
  # la table contient les iris et des epci factices de com insulaires avec id_ter de type ZZZZ12345 à écarter, 
  # les millesimes avant 2012 n'ont pas été renseignés par Enedis
  filter(!grepl("ZZZ", id_ter), echelle != "iris", as.character(annee) > "2011") %>% 
  # Resolution du problème d'encodage sur les TypeZone
  mutate(echelle = case_when(echelle == "commune" ~ grep("com", levels(liste_zone$TypeZone), value = TRUE, ignore.case = TRUE),
                             echelle == "département" ~ grep("d.p", levels(liste_zone$TypeZone), value = TRUE, ignore.case = TRUE),
                             echelle == "région" ~ grep("r.g", levels(liste_zone$TypeZone), value = TRUE, ignore.case = TRUE),
                             echelle == "epci" ~ grep("epci", levels(liste_zone$TypeZone), value = TRUE, ignore.case = TRUE),
                             TRUE ~ as.character(echelle)) %>% as.factor())


# commentaires sur dessin fichiers -----------
# Le df conso_elec_179_1 comprend les consommations électriques annuelles à toutes les échelles sauf iris depuis 2008.
# Il correspond à la mise aux référentiels géographiques actuels de l'ensemble de la source *consommations électriques art 179* publiée par le SDES. 
# En raison de la manière de gérer le secret, il ne faut pas utiliser les consommations communales ou à l'iris pour déduire les consommations supra, 
# mais utiliser directement les consommations agrégées à l'échelle EPCI, département ou région. Les conso Fce peuvent être calculées par somme des régions


# ajout total France ---------

source("R/aggreg_non_sommable/aggreg_conso_179.R")

conso_elec_179_frmetro <- filter(conso_elec_179_1, echelle == grep("r.g", levels(liste_zone$TypeZone), value = TRUE, ignore.case = TRUE),
                                 !(id_ter %in% c("01", "02", "03", "04", "05", "06"))) %>%
  group_by(annee) %>%
  aggr_ter_conso() %>%
  mutate(echelle="France", id_ter="FRMETRO", lib_ter="France métropolitaine", sel_reg = FALSE)

conso_elec_179_frentiere <- filter(conso_elec_179_1, echelle == grep("r.g", levels(liste_zone$TypeZone), value = TRUE, ignore.case = TRUE)) %>%
  group_by(annee) %>%
  aggr_ter_conso() %>%
  mutate(echelle="France", id_ter="FRMETRODROM", lib_ter="France métropolitaine et DROM", sel_reg = FALSE)

conso_elec_179_2 <- bind_rows(conso_elec_179_1, conso_elec_179_frmetro, conso_elec_179_frentiere) %>% 
  mutate(date = make_date(annee, 12,31)) %>%
  mutate_if(is.character, as.factor) %>%
  rename(TypeZone = echelle, Zone = lib_ter, CodeZone = id_ter, conso_elec_gwh = conso_gwh)

# rm(conso_elec_179_frentiere, conso_elec_179_frmetro)


# gestion du champ 'sources' à part ----------
# on isole les métadonnées dans un RData à part
metad_conso_elec_179 <- select(conso_elec_179_2, ends_with("Zone"), date, sources, sce_estim)
save(metad_conso_elec_179, file="metadata/metad_conso_elec_179.RData")
# rm(metad_conso_elec_179)


# reprise du calcul ------
levels_communes <- select(communes, CodeZone = DEPCOM, Zone = NOM_DEPCOM) %>% 
  mutate_if(is.factor, as.character) %>% 
  mutate(TypeZone = grep("com", levels(liste_zone$TypeZone), value = T, ignore.case = T)) %>% 
  unite(id_zone, CodeZone, TypeZone, Zone, sep="__") %>%
  pull

conso_elec_179 <- conso_elec_179_2 %>%
  rename_with( ~ gsub("na$","_elec.inconnu", .x)) %>%            
  rename_with( ~ gsub("a$","_elec.agriculture", .x)) %>%
  rename_with( ~ gsub("i$","_elec.industrie", .x)) %>%
  rename_with( ~ gsub("t$","_elec.tertiaire", .x)) %>%            
  rename_with( ~ gsub("r$","_elec.residentiel", .x)) %>%            
  unite(id_zone, CodeZone, TypeZone, Zone, sep="__") %>%
  pivot_longer(names_to = "variable", values_to = "valeur", 'conso_elec.agriculture':conso_elec_gwh) %>%
  mutate(id_zone = fct_expand(id_zone, levels_communes)) %>% 
  complete(id_zone, date, variable, fill = list(valeur = NA)) %>%
  separate(id_zone, c("CodeZone", "TypeZone", "Zone"), sep="__") %>%
  select(date, CodeZone, TypeZone, Zone, variable, valeur) %>%
  pivot_wider(names_from = variable, values_from = valeur) %>% 
  mutate_if(is.factor, as.character) 
# save(conso_elec_179, file = "sysdata/conso_elec_179.RData")  


# versement et documentation dans le sgbd/datamart.portrait_territoires -------------

source("R/poster_documenter_ind.R", encoding = "UTF-8")
poster_documenter_ind(df = conso_elec_179,
                     nom_table_sgbd = "cogifiee_conso_elec_179", 
                     comm_source_en_plus = "T\u00e9l\u00e9chargement depuis DIDO https://www.statistiques.developpement-durable.gouv.fr/donnees-locales-de-consommation-denergie#lectricit.")

rm(list=ls())
