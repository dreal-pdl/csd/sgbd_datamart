
# chargement_eptb


# librairies --------
library(tidyverse)
library(readxl)
library(tricky)
library(lubridate)
library(DBI)
library(RPostgreSQL)
library(datalibaba)
library(googlesheets4)

rm(list = ls())


# chargement et calcul ----------
# chargement des données EPTB (Enquete sur le prix des terrains à batir) --------
# fichier cree a partir d une requete geokit 3 :
# "Dossiers publics/geokit3/Regions Pays de la Loire/DREAL/INDICATEURS TER DATA LAB/gk3_eptb"
eptb <-bind_rows(read_excel("extdata/gk3_eptb.xlsx",sheet=1),
                 read_excel("extdata/gk3_eptb.xlsx",sheet=2),
                 read_excel("extdata/gk3_eptb.xlsx",sheet=3)) %>% 
  rename(date = 'eptb - Année',
         depcom = 'eptb - Code de la commune',
         nombre_d_observations_non_pondere='eptb - Nombre d observations (non pondéré)',
         nombre_de_maisons_terrain_achete_ou_non='eptb - Nombre de maisons (terrain acheté ou non)',
         prix_total_des_maisons_en_euros='eptb - Prix total des maisons (en euros)',
         surface_totale_des_maisons_en_m2='eptb - Surface totale des maisons (en m2)',
         nombre_de_terrains_achetes='eptb - Nombre de terrains achetés',
         prix_total_des_maisons_en_euros_avec_terrain_achete='eptb - Prix total des maisons (en euros) avec terrain achete',
         prix_total_des_terrains_en_euros='eptb - Prix total des terrains (en euros)',
         superficie_totale_des_terrains_achetes_en_m2='eptb - Superficie totale des terrains achetés (en m²)') %>%
  gather(variable,valeur,nombre_d_observations_non_pondere:superficie_totale_des_terrains_achetes_en_m2) %>% 
  select(depcom,date,variable,valeur) %>% 
  filter(date>=2008) %>%
  mutate(date=make_date(date,12,31))%>% 
  mutate_if(is.character,as.factor) %>% 
  complete(depcom,date,variable,fill = list(valeur =0)) %>% 
  pivot_wider(names_from = variable,values_from = valeur) %>%
  filter(depcom != "97123")

# versement dans le sgbd/datamart.portrait_territoires -------------
poster_data(data = eptb,
            db = "datamart",
            schema = "portrait_territoires", 
            table = "source_eptb",
            post_row_name = FALSE, 
            overwrite = TRUE,
            droits_schema = TRUE,
            pk = c("depcom", "date"), # déclaration d'une clé primaire sur la table postée : on ne doit pas avoir deux lignes avec à la fois le même code commune et la meme date
            user = "does")


# METADONNEES------------------------------------

## On récupère la liste des variables qui sont à documenter dans le tableur google sheet à partir du jeu de données posté
var <- setdiff(names(eptb), c("depcom", "date"))

## récupération du nom du présent script source pour filtrer ensuite le référentiel des indicateurs
nom_script_sce <- rstudioapi::getActiveDocumentContext()$path %>% # utilisation de rstudioapi pour récupérer le nom du présent script 
  basename() %>% # on enlève le chemin d'accès pour ne garder que le nom du fichier
  gsub(pattern = ".R$", "", .) # on enlève l'extension '.R'

## authentification google sheet grâce au .Renviron
gs4_auth_configure(api_key = Sys.getenv("google_api_key"))
gs4_deauth()



## chargement du référentiel indicateurs google sheet
metadata_indicateur <- read_sheet("https://docs.google.com/spreadsheets/d/1n-dhtrJM3JwFVz5WSEGOQzQ8A0G7VT_VcxDe5gh6zSo/edit#gid=60292277",
                                  sheet = "indicateurs") %>%
  # on ne garde que les variables concernées par le présent script de chargement
  filter(source == nom_script_sce) %>% 
  # on ajoute l'unité dans le libellé de la variable
  mutate(libelle_variable = paste0(libelle_variable, " (unit\u00e9 : ", unite, ")")) %>% 
  select(variable, libelle_variable) %>% 
  # ajout des libellés pour depcom et date
  bind_rows(
    tribble(
      ~variable, ~libelle_variable,
      "depcom", "Code INSEE de la commune",
      "date", "Millesime"
    )
  )

1## Vérification que la documentation des indicateurs est complète
all(var %in% metadata_indicateur$variable) # doit renvoyer TRUE

## Envoi des libellés de variable dans le SGBD
post_dico_attr(dico = metadata_indicateur, table = "source_eptb", schema = "portrait_territoires",
               db = "datamart", user = "does")

## Récupération des métadonnées de la source
nom_sce <- str_replace(nom_script_sce, "chargement_|ref_|specifique_", "") %>%
  str_replace("indicateur_", "") %>%
  str_replace("_cogiter|_cog$", "")

metadata_source <- read_sheet("https://docs.google.com/spreadsheets/d/1n-dhtrJM3JwFVz5WSEGOQzQ8A0G7VT_VcxDe5gh6zSo/edit#gid=60292277",
                              sheet = "sources") %>%
  filter(source == nom_sce) %>% 
  mutate(com_table = paste0(source_lib, " - ", producteur, ".\n", descriptif_sources)) %>% 
  pull(com_table) %>%
  # ajout de complement sur la généalogie
  paste0(".\n", "Chargement des donn\u00e9es sur Geokit3")

## commentaires de la table

commenter_table(comment = metadata_source,
                db = "datamart",
                schema = "portrait_territoires",
                table = "source_eptb", 
                user = "does")

