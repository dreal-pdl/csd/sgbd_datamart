
# chargement_etat_civil

# librairies --------
library(tidyverse)
library(readxl)
library(tricky)
library(lubridate)
library(DBI)
library(RPostgreSQL)
library(datalibaba)
library(googlesheets4)

rm(list=ls())


#données Etat Civil issues du site de l'INSEE
# fichiers :  naissances 2014 à 2023 : https://insee.fr/fr/statistiques/1893255
# décés 2014 à 2022 : https://insee.fr/fr/statistiques/1893253
# sélection des fichiers sur https://insee.fr/fr/statistiques ???


# paramètres ----------
mil <- 2023


# chargement --------------
etat_civil_naissances <- read_excel(paste0("extdata/base_naissances_",mil,".xlsx"),sheet=2, skip=3) %>% 
  set_standard_names() 
etat_civil_deces <- read_excel(paste0("extdata/base_deces_",mil,".xlsx"),sheet=2, skip=3) %>% 
  set_standard_names() 


# calcul ---------
etat_civil_naissances <- etat_civil_naissances %>%
  rename(depcom=1) %>%
  gather(date,valeur,3:ncol(etat_civil_naissances))%>%
  mutate(variable="nb_naissances") %>% 
  select(c(depcom,date,variable,valeur))
etat_civil_naissances$date <- str_replace(etat_civil_naissances$date,"var_","")

etat_civil_deces <- etat_civil_deces %>%
  rename(depcom=1) %>% 
  gather(date,valeur,3:ncol(etat_civil_deces))%>%
  mutate(variable="nb_deces") %>% 
  select(c(depcom,date,variable,valeur))
etat_civil_deces$date <- str_replace(etat_civil_deces$date,"var_","")

etat_civil<- bind_rows(etat_civil_naissances,etat_civil_deces)%>%
  complete(depcom,date,variable,fill = list(valeur =0)) %>%
  mutate(date=make_date(date,12,31))%>% 
  mutate_if(is.character, as.factor) %>% 
  pivot_wider(names_from = variable,values_from = valeur)

# suppression des lignes de metadonnees
etat_civil<- etat_civil %>% 
  mutate(depcom = as.character(depcom)) %>% 
  mutate(meta = nchar(depcom)) %>% 
  filter (meta < 6) %>% 
  select(-meta) %>% 
  mutate_if(is.character, as.factor)


# # versement dans le sgbd/datamart.portrait_territoires -------------
# drv <- dbDriver("PostgreSQL")
# con_datamart <- dbConnect(drv, 
#                           dbname="datamart", 
#                           host=Sys.getenv("server"), 
#                           port=Sys.getenv("port"),
#                           user=Sys.getenv("userid"),
#                           password=Sys.getenv("pwd_does"))
# postgresqlpqExec(con_datamart, "SET client_encoding = 'windows-1252'")
# 
# dbWriteTable(con_datamart, c("portrait_territoires","source_etat_civil"),
#              etat_civil, row.names=FALSE, overwrite=TRUE)
# 
# dbDisconnect(con_datamart)
# 
# rm(list=ls())


# versement dans le sgbd/datamart.portrait_territoires -------------
poster_data(data = etat_civil,
            db = "datamart",
            schema = "portrait_territoires", 
            table = "source_etat_civil",
            post_row_name = FALSE, 
            overwrite = TRUE,
            droits_schema = TRUE,
            pk = c("depcom", "date"), # déclaration d'une clé primaire sur la table postée : on ne doit pas avoir deux lignes avec à la fois le même code commune et la meme date
            user = "does")

# METADONNEES------------------------------------

## On récupère la liste des variables qui sont à documenter dans le tableur google sheet à partir du jeu de données posté
var <- setdiff(names(etat_civil), c("depcom", "date"))

## récupération du nom du présent script source pour filtrer ensuite le référentiel des indicateurs
nom_script_sce <- rstudioapi::getActiveDocumentContext()$path %>% # utilisation de rstudioapi pour récupérer le nom du présent script 
  basename() %>% # on enlève le chemin d'accès pour ne garder que le nom du fichier
  gsub(pattern = ".R$", "", .) # on enlève l'extension '.R'

## authentification google sheet grâce au .Renviron
gs4_auth_configure(api_key = Sys.getenv("google_api_key"))
gs4_deauth()



## chargement du référentiel indicateurs google sheet
metadata_indicateur <- read_sheet("https://docs.google.com/spreadsheets/d/1n-dhtrJM3JwFVz5WSEGOQzQ8A0G7VT_VcxDe5gh6zSo/edit#gid=60292277",
                                  sheet = "indicateurs") %>%
  # on ne garde que les variables concernées par le présent script de chargement
  filter(source == nom_script_sce) %>% 
  # on ajoute l'unité dans le libellé de la variable
  mutate(libelle_variable = paste0(libelle_variable, " (unit\u00e9 : ", unite, ")")) %>% 
  select(variable, libelle_variable) %>% 
  # ajout des libellés pour depcom et date
  bind_rows(
    tribble(
      ~variable, ~libelle_variable,
      "depcom", "Code INSEE de la commune",
      "date", "Millesime"
    )
  )

## Vérification que la documentation des indicateurs est complète
all(var %in% metadata_indicateur$variable) # doit renvoyer TRUE

## Envoi des libellés de variable dans le SGBD
post_dico_attr(dico = metadata_indicateur, table = "source_etat_civil", schema = "portrait_territoires",
               db = "datamart", user = "does")

## Récupération des métadonnées de la source
nom_sce <- str_replace(nom_script_sce, "chargement_|ref_|specifique_", "") %>%
  str_replace("indicateur_", "") %>%
  str_replace("_cogiter|_cog$", "")

metadata_source <- read_sheet("https://docs.google.com/spreadsheets/d/1n-dhtrJM3JwFVz5WSEGOQzQ8A0G7VT_VcxDe5gh6zSo/edit#gid=60292277",
                              sheet = "sources") %>%
  filter(source == nom_sce) %>% 
  mutate(com_table = paste0(source_lib, " - ", producteur, ".\n", descriptif_sources)) %>% 
  pull(com_table) %>%
  # ajout de complement sur la généalogie
  paste0("\n", "T\u00e9l\u00e9chargement depuis le site de l\'Insee https://www.insee.fr/fr/statistiques/1893255 et https://www.insee.fr/fr/statistiques/1893253. Naissances et d\u00e9c\u00e9s 2014-2020")

## commentaires de la table

commenter_table(comment = metadata_source,
                db = "datamart",
                schema = "portrait_territoires",
                table = "source_etat_civil", 
                user = "does")


