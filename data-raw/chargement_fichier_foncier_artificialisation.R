
# chargement_fichier_foncier_artificialisation


library(magrittr)

rm(list = ls())


# chargement de la liste des depcom COGiter région PDL et région PDL + epci-----------
source("R/levels_facteurs_com.R") 


# chargement des données et calculs -----------

base <- "fichiersfonciers"

## 2016 à 2017 -------------

# 2016 à 2017 : annee, departement
# base <- "fichiersfonciers"
# schema_2016 <- ff_annexes_tup_2016
# table_44_2016 <- d44_2016_tup
# table_49_2016 <- d49_2016_tup

annee_max_1 <- 2017

creer_artificialisation_1 <- function(annee, departement){
  schema <- paste0("ff_annexes_tup_", annee)
  tab <- paste0("d", departement,"_", annee, "_tup")
  
  d_tup <- datalibaba::importer_data(
    db = base, schema = schema, table = tab
    ) %>%
    sf::st_drop_geometry() %>% 
    dplyr::select(c(idtup, idcom, dcntpa, dcntarti)) %>%
    dplyr::collect()
  
  d_artificialisation <- d_tup %>% 
    dplyr::group_by(idcom) %>% 
    dplyr::summarise(
      surf_cadastree = round((sum(dcntpa)/10000), digits = 2),
      surf_artificialisee = round((sum(dcntarti)/10000), digits = 2)
      ) %>% # conversion de m² en ha
    dplyr::ungroup() %>%
    dplyr::mutate(annee = annee)
  
  return(d_artificialisation)
  }
 
# d44_2016_tup <- creer_artificialisation_1(2016, 44) # test
# names(d44_2016_tup)
# [1] "idcom"          "surf_cadastree"   "surf_artificialisee" "annee"

r52_2016_2017_artificialisation <- tidyr::crossing(c(2016:annee_max_1), c(44,49,53,72,85)) %>%
  purrr::pmap(~creer_artificialisation_1(annee = .x, departement = .y))
r52_2016_2017_artificialisation <- dplyr::bind_rows(r52_2016_2017_artificialisation)
 

# 2018 à 2023 -------------

# 2018 à 2023 : annee
# base <- "fichiersfonciers"
# schema_2018 <- "ff_2018"
# table_2018 <- "ffta_2018_tup"

annee_max_2 <- 2023

creer_artificialisation_2 <- function(annee){
  schema <- paste0("ff_",annee)
  table <- paste0("ffta_",annee,"_tup")
  
  ffta_tup <- datalibaba::importer_data(
    db = base, schema = schema, table = table
    ) %>%
    sf::st_drop_geometry() %>% 
    dplyr::select(c(idtup, idcom, dcntpa, dcntarti)) %>%
    dplyr::collect()
  
  ffta_artificialisation <- ffta_tup %>%
    dplyr::group_by(idcom) %>% 
    dplyr::summarise(
      surf_cadastree = round((sum(dcntpa)/10000), digits = 2),
      surf_artificialisee = round((sum(dcntarti)/10000), digits = 2)
      ) %>% # conversion de m² en ha
    dplyr::ungroup() %>%
    dplyr::mutate(annee = annee)
  
  return(ffta_artificialisation)
  }

# ffta_2018_tup <- creer_artificialisation_2(2018) # test
# names(ffta_2018_tup)
# [1] "idcom"          "surf_cadastree"   "surf_artificialisee" "annee"

# r52_2018_2022_artificialisation <- purrr::map_dfr(c(2018:annee_max_2), creer_artificialisation_2)
# méthode alternative :
ffta_2018_tup <- creer_artificialisation_2(2018)
ffta_2019_tup <- creer_artificialisation_2(2019)
ffta_2020_tup <- creer_artificialisation_2(2020)
ffta_2021_tup <- creer_artificialisation_2(2021)
ffta_2022_tup <- creer_artificialisation_2(2022)
ffta_2023_tup <- creer_artificialisation_2(2023)

r52_2018_2023_artificialisation <- dplyr::bind_rows(
  ffta_2018_tup,
  ffta_2019_tup,
  ffta_2020_tup,
  ffta_2021_tup,
  ffta_2022_tup,
  ffta_2023_tup
  )


# aggrégation de 2016 à 2023 -----------
r52_2016_2023_artificialisation <- dplyr::bind_rows(
  r52_2016_2017_artificialisation,
  r52_2018_2023_artificialisation
  )


# calcul de l'indicateur ----------
source_fichier_foncier_artificialisation <- r52_2016_2023_artificialisation %>%
  dplyr::rename(depcom = idcom) %>% 
  dplyr::mutate(depcom = as.factor(depcom)) %>% 
  dplyr::mutate(date = lubridate::dmy(paste("01-01", annee, sep = "-"))) %>%
  dplyr::select(depcom, date, surf_cadastree, surf_artificialisee) %>% 
  dplyr::mutate(depcom = forcats::fct_expand(depcom, com_reg_et_vois)) %>% 
  tidyr::complete(depcom, date, fill = list(surf_cadastree = NA , surf_artificialisee = NA)) %>%
  dplyr::mutate(dep = substr(depcom, 1, 2)) %>%
  dplyr::mutate (
    surf_cadastree = dplyr::case_when(
      dep %in% c("44","49","53","72","85") & is.na(surf_cadastree)  ~ 0 ,
      TRUE ~ surf_cadastree
      ),
    surf_artificialisee = dplyr::case_when(
    dep %in% c("44","49","53","72","85") & is.na(surf_artificialisee)  ~ 0 ,
    TRUE ~ surf_artificialisee
    )
  ) %>% 
  dplyr::select(-dep)


# versement dans le sgbd/datamart.portrait_territoires et metadonnées -------------
source("R/poster_documenter_data.R", encoding = "UTF-8")
poster_documenter_it(
  df = source_fichier_foncier_artificialisation ,
  nom_table_sgbd = "source_fichier_foncier_artificialisation",
  comm_source_en_plus = ""
  )

