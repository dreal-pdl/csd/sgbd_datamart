
# chargement_pci

library(magrittr)

rm(list = ls())


# connection au SGBD -----------
con_referentiels <- DBI::dbConnect(
  RPostgres::Postgres(), 
  dbname="referentiels",
  host=Sys.getenv("server"),
  port=Sys.getenv("port"),
  user=Sys.getenv("userid"),
  password=Sys.getenv("pwd_does")
  )


# fonction de calcul de l'indicateur emprise au sol du bâti, dur ou léger ---------------
creer_emprise_bati <- function(annee, departement){
  
  query = paste0("select * from plan_cadastral_informatise.n_pci_batiment_", annee, "_0", departement)
  n_pci_batiment <- sf::st_read(con_referentiels, query = query)
  
  pci <- n_pci_batiment %>%
    dplyr::mutate(surf_bati = sf::st_area(.)) %>%
    dplyr::group_by(insee, dur, dur_code, surf_bati) %>% 
    unique() %>%
    dplyr::ungroup() %>% 
    sf::st_drop_geometry()
  pci$surf_bati <- units::drop_units(pci$surf_bati)
  
  pci_1 <- pci %>% 
    dplyr::group_by(insee) %>% 
    dplyr::summarise(surf_bati_total = round(sum(surf_bati)/10000, digits = 2)) %>%
    dplyr::ungroup()
  pci_2 <- pci %>% 
    dplyr::filter(dur_code == "01") %>% # problème avec variable dur, car libellé non constant selon les années
    dplyr::group_by(insee) %>% 
    dplyr::summarise(surf_bati_dur = round(sum(surf_bati)/10000, digits = 2)) %>% 
    dplyr::ungroup()
  pci_3 <- pci %>%
    dplyr::filter(dur_code == "02") %>% 
    dplyr::group_by(insee) %>% 
    dplyr::summarise(surf_bati_leger = round(sum(surf_bati)/10000, digits = 2)) %>%
    dplyr::ungroup()
  
  d_emprise_bati <- pci_1 %>% 
    dplyr::inner_join(pci_2) %>% 
    dplyr::inner_join(pci_3) %>% 
    dplyr::mutate(annee = annee)
  
  return(d_emprise_bati)
}


# chargement de la liste des depcom COGiter région PDL et région PDL + epci ----------
source("R/levels_facteurs_com.R")


# calcul de l'indicateur ---------

annee_max <- 2020
# année 2016 non disponible
# années 2013 et 2015 incomplètes 

r52_2011_2020_emrpise_bati <- tidyr::crossing(
  c(2011,2012,2014,2017:annee_max), 
  c(44,49,53,72,85)
  ) %>% 
  purrr::pmap(~creer_emprise_bati(annee = .x, departement = .y))
r52_2011_2020_emrpise_bati <- dplyr::bind_rows(r52_2011_2020_emrpise_bati)

r52_2013_emrpise_bati <- purrr::map2_dfr(2013, c(44,49,53,72), ~creer_emprise_bati(2013,.y))

r52_2015_emrpise_bati <- purrr::map2_dfr(2015, c(44,85), ~creer_emprise_bati(2015,.y))

pci_pdl <- dplyr::bind_rows(
  r52_2011_2020_emrpise_bati,
  r52_2013_emrpise_bati,
  r52_2015_emrpise_bati)

source_pci <- pci_pdl %>% 
  dplyr::rename(depcom = insee) %>% 
  dplyr::mutate(depcom = as.factor(depcom)) %>% 
  dplyr::mutate(date = lubridate::dmy(paste("01-01",annee,sep = "-"))) %>% 
  dplyr::select(-annee) %>% 
  tidyr::pivot_longer(cols = surf_bati_total:surf_bati_leger, names_to = "variable", values_to = "valeur") %>% 
  dplyr::mutate(depcom = forcats::fct_expand(depcom, com_reg)) %>%
  tidyr::complete(depcom, date, variable, fill = list(valeur = 0), explicit = FALSE) %>% 
  dplyr::mutate(depcom = forcats::fct_expand(depcom, com_reg_et_vois)) %>%
  tidyr::complete(depcom, date, variable, fill = list(valeur = NA)) %>%   
  tidyr::pivot_wider(names_from = variable, values_from = valeur)


# versement dans le sgbd/datamart.portrait_territoires et metadonnées -------------
source("R/poster_documenter_data.R", encoding = "UTF-8")
poster_documenter_it(df = source_pci ,
                     nom_table_sgbd = "source_pci", 
                     comm_source_en_plus = "")

