
# chargement_protection_naturelle

library(magrittr)

rm(list=ls())

# importer r52, car toutes les tables ne sont pas disponibles France entière sur le SGBD
# => Epci à cheval sur plusieurs région : valeur à NA


# chargement des données du SGBD ------
n_enp_rnn_s_r52 <- datalibaba::importer_data(
  db = "consultation",
  schema = "nature_biodiversite",
  table = "n_enp_rnn_s_r52"
  ) # la table n_enp_rnn_s_000 existe
n_enp_rnr_s_r52 <- datalibaba::importer_data(
  db = "consultation",
  schema = "nature_biodiversite",
  table = "n_enp_rnr_s_r52"
  ) # la table n_enp_rnr_s_000 existe
n_enp_rb_s_r52 <- datalibaba::importer_data(
  db = "consultation",
  schema = "nature_biodiversite",
  table = "n_enp_rb_s_r52"
  ) # la table n_enp_rb_s_000 existe
n_enp_apb_s_r52 <- datalibaba::importer_data(
  db = "consultation",
  schema = "nature_biodiversite",
  table = "n_enp_apb_s_r52"
  ) # la table n_enp_apb_s_000 existe
n_enp_apg_s_r52 <- datalibaba::importer_data(
  db = "consultation",
  schema = "nature_biodiversite",
  table = "n_enp_apg_s_r52"
  ) # pas de table France entière
n_enp_rcfs_s_r52 <- datalibaba::importer_data(
  db = "consultation",
  schema = "nature_biodiversite",
  table = "n_enp_rcfs_s_r52"
  ) # pas de table France entière
n_enp_scl_s_r52 <- datalibaba::importer_data(
  db = "consultation",
  schema = "nature_biodiversite",
  table = "n_enp_scl_s_r52"
  ) # la table n_enp_scl_s_000 existe
n_enp_pnr_s_r52 <- datalibaba::importer_data(
  db = "consultation",
  schema = "nature_biodiversite",
  table = "n_enp_pnr_s_r52"
  ) # la table n_enp_pnr_s_000 existe
r_site_natura_2000_table_r52 <- datalibaba::importer_data(
  db = "consultation",
  schema = "nature_biodiversite",
  table = "r_site_natura_2000_table_r52"
  ) # pas de table France entière
n_enp_ramsar_s_r52 <- datalibaba::importer_data(
  db = "consultation",
  schema = "nature_biodiversite",
  table = "n_enp_ramsar_s_r52"
  ) # pas de table France entière
r_patrimoine_mondial_r52 <- datalibaba::importer_data(
  db = "consultation",
  schema = "site_paysage",
  table = "r_patrimoine_mondial_r52"
  ) # pas de table France entière

# limites administratives
com <- datalibaba::importer_data(
  db = "referentiels",
  schema = "bdtopo_v3",
  table = "n_bdt_commune_s_r52"
  ) %>% 
  dplyr::select(
    code_insee, 
    nom_com = nom_officiel
    ) %>% 
  dplyr::mutate(surf_calculee_en_m2 = sf::st_area(the_geom))


# import de shp pour sites CEN -------------

# courriel de Thomas OBE, DREAL PDL/SRNP/DB du 2021-01-28 :
# les sites acquis et gérés des CEN ne remontent pas obligatoirement par la base DREAL,
# ils vont directement sur la base INPN du Museum. On les trouve sur leur site avec les surfaces correspondantes. 
# On peut faire une demande pour obtenir les couches SIG. 
# Ou alors directement au CEN, le directeur est Franck Boitard : f.boitard@cenpaysdelaloire.fr
# Envoie de la couche nationale (shp) disponible fin 2020 sur le site INPN, mais peut-être pas tout à fait complète
# (quelques sites peuvent avoir été ajoutés en 2020 et non encore saisis dans la base, à la marge).

utils::download.file(
  url = "https://inpn.mnhn.fr/docs/Shape/cen.zip", 
  destfile = "extdata/cen.zip"
  )
utils::unzip(
  zipfile = 'extdata/cen.zip',
  exdir = "extdata"
  )
# n_epn_scen_s_000 <- sf::st_read(
#   dsn = "extdata/scen2018_12",
#   layer='N_ENP_SCEN_S_000'
#   )
n_epn_scen_s_sa <- sf::st_read(
  dsn = "extdata/cen",
  layer='N_ENP_SCEN_S_SA'
)


# protections fortes ---------
x <- sf::st_union(
  n_enp_rnn_s_r52 %>% dplyr::select(id_local),
  n_enp_rnr_s_r52 %>% dplyr::select(id_local)
  )
x <- dplyr::summarise(x, do_union = TRUE)
x <- sf::st_union(
  x,
  n_enp_rb_s_r52 %>% dplyr::select(id_local)
  )
x <- dplyr::summarise(x, do_union = TRUE)
x<-sf::st_union(
  x,
  n_enp_apb_s_r52%>% dplyr::select(id_local)
  )
x <- dplyr::summarise(x, do_union = TRUE)
protection_forte <- sf::st_union(
  x,
  n_enp_apg_s_r52 %>% dplyr::select(id_local)
  )
protection_forte <- dplyr::summarise(protection_forte, do_union = TRUE)
rm(x)


# protections 'élargies' ---------
x <- sf::st_union(
  protection_forte,
  n_enp_rcfs_s_r52 %>% dplyr::select(id_local)
  )
x <- dplyr::summarise(x, do_union = TRUE)
x <- sf::st_union(
  x,
  sf::st_buffer(n_enp_scl_s_r52 %>% dplyr::select(id_local), 0)
  )
x <- dplyr::summarise(x, do_union = TRUE)
x<- sf::st_union(
  x,
  sf::st_buffer(n_epn_scen_s_sa %>% dplyr::select(ID_LOCAL), 0)
  )
x <- dplyr::summarise(x, do_union = TRUE)
x <- sf::st_union(
  x,
  n_enp_pnr_s_r52 %>% dplyr::select(id_local)
  )
x <- dplyr::summarise(x, do_union = TRUE)
x <- sf::st_union(
  x,
  r_site_natura_2000_table_r52 %>% dplyr::select(id_mnhn)
  )
x <- dplyr::summarise(x, do_union = TRUE) # très très long
x <- sf::st_union(
  x,
  n_enp_ramsar_s_r52 %>% dplyr::select(id_local)
  )
x <- dplyr::summarise(x, do_union = TRUE)
protection_elargie <- sf::st_union(
  x,
  r_patrimoine_mondial_r52 %>% dplyr::select(id_bien)
  )
protection_elargie <- dplyr::summarise(protection_elargie, do_union = TRUE)
rm(x)


# intersection des protections et des communes ---------
intersection_protection_forte_commune <- sf::st_intersection(
  sf::st_buffer(protection_forte, 0),
  sf::st_buffer(com, 0)
  ) %>% 
  dplyr::mutate(surf_protection_forte = sf::st_area(the_geom)) %>% 
  sf::st_drop_geometry() %>% 
  dplyr::right_join(com %>% sf::st_drop_geometry()) %>%
  dplyr::mutate(surf_protection_forte = ifelse(is.na(surf_protection_forte), 0, surf_protection_forte))
intersection_protection_forte_commune$surf_calculee_en_m2 <- units::drop_units(intersection_protection_forte_commune$surf_calculee_en_m2)

intersection_protection_elargie_commune <- sf::st_intersection(
  sf::st_buffer(protection_elargie, 0), 
  sf::st_buffer(com, 0)
  ) %>% 
  dplyr::mutate(surf_protection_elargie = sf::st_area(the_geom)) %>% 
  sf::st_drop_geometry() %>% 
  dplyr::right_join(com %>% sf::st_drop_geometry()) %>%
  dplyr::mutate(surf_protection_elargie = ifelse(is.na(surf_protection_elargie), 0, surf_protection_elargie))
intersection_protection_elargie_commune$surf_calculee_en_m2 <- units::drop_units(intersection_protection_elargie_commune$surf_calculee_en_m2)


# calcul de l'indicateur -----------
source_protection_naturelle_0 <- dplyr::left_join(
  intersection_protection_forte_commune,
  intersection_protection_elargie_commune
  ) %>% 
  dplyr::select(-nom_com) %>% 
  dplyr::rename(depcom = code_insee) %>% 
  dplyr::mutate(date = as.Date("2023-07-01")) %>%  
  # les mises à jour des tables de protections naturelles s'effectuent en continue selon SIGLOIRE
  dplyr::mutate_if(is.character,as.factor) %>% 
  dplyr::select(depcom, date, tidyselect::everything())

# chargement de la liste des depcom COGiter région et région + epci
source("R/levels_facteurs_com.R")

source_protection_naturelle <- source_protection_naturelle_0 %>% 
  tidyr::pivot_longer(
    cols = surf_calculee_en_m2:surf_protection_elargie, 
    names_to = "variable", 
    values_to = "valeur"
    ) %>%  
  dplyr::mutate_if(is.character, as.factor) %>%
  dplyr::mutate(depcom = forcats::fct_expand(depcom, com_reg)) %>%
  tidyr::complete(depcom, date, variable, fill = list(valeur = 0), explicit = FALSE) %>%
  dplyr::mutate(depcom = forcats::fct_expand(depcom, com_reg_et_vois)) %>%
  tidyr::complete(depcom, date, variable, fill = list(valeur = NA)) %>% 
  dplyr::arrange(date, depcom) %>%
  tidyr::pivot_wider(names_from = variable, values_from = valeur)
  

# versement dans le sgbd/datamart.portrait_territoires et metadonnées -------------
source("R/poster_documenter_data.R", encoding = "UTF-8")
poster_documenter_it(df = source_protection_naturelle ,
                     nom_table_sgbd = "source_protection_naturelle", 
                     comm_source_en_plus = "")

