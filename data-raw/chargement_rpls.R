
# chargement_rpls

rm(list = ls())

library(magrittr) # pour le %>% 

source(file = "R/dataprep_all.R")

rpls_prep <- dataprep_all() %>% 
  dplyr::filter(!is.na(millesime))

# # liste des communes des EPCI de la région Pays de la Loire
# communes_region52 <- COGiter::communes %>%
#   dplyr::filter(grepl("52", REGIONS_DE_L_EPCI)) %>%
#   dplyr::mutate(DEPCOM = as.character(DEPCOM))  %>%
#   dplyr::pull(DEPCOM)

rpls <- rpls_prep %>%
  # dplyr::filter(TypeZone %in% c("Départements", "Régions", "Epci", "France") | CodeZone %in% communes_region52 ) %>%
  dplyr::mutate(
    date = lubridate::make_date(as.character(millesime), 01, 01),
    dplyr::across(tidyselect::where(is.character), as.factor)
    ) %>%
  dplyr::select(-millesime) %>%
  dplyr::mutate_if(is.factor, as.character)

# versement et documentation dans le sgbd/datamart.portrait_territoires ------
source("R/poster_documenter_ind.R", encoding = "UTF-8")
poster_documenter_ind(
  df = rpls,
  nom_table_sgbd = "cogifiee_rpls", 
  comm_source_en_plus = "Chargement des donn\u00e9es sur le package propre.rpls"
  )

rm(list = ls()) 
