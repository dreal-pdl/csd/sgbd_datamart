
# chargement_sitadel

# librairies ---------
library(readxl)
library(tidyverse)
library(DT)
library(tricky)
library(lubridate)

rm(list = ls())


# millesimes à importer ----------
millesimes <- c(2009,2010,2011,2012,2013,2014,2015,2016,2017,2018,2019,2020,2021,2022,2023)


# fonction de chargement des données Sitadel --------
# fichier cree a partir d une requete geokit 3 :
# "Dossiers publics/geokit3/Regions Pays de la Loire/DREAL/INDICATEURS TER DATA LAB/gk3_sitadel"

creer_millesimes<-function(millesimes){
  
  sitadel_logement <- read_excel(paste0("extdata/gk3_sitadel_",millesimes,".xlsx"),sheet=1) %>% 
    set_standard_names() %>% 
    rename(date = lgt_annee,
           depcom = lgt_code_de_la_commune)
  
  sitadel_locaux <- read_excel(paste0("extdata/gk3_sitadel_",millesimes,".xlsx"),sheet=2) %>% 
    set_standard_names() %>%
    rename(date=loc_annee,
           depcom=loc_code_de_la_commune)
  
  sitadel <- bind_rows(sitadel_logement %>% 
                         gather(variable,valeur,nb_lgt_aut_ordinaires:nb_lgt_com_total),
                       sitadel_locaux %>% 
                         gather(variable,valeur,surface_aut_creee_totale_des_locaux:surface_com_creee_loc_total_service_public)) %>%
    select(depcom,date,variable,valeur) %>% 
    mutate(date=make_date(date,12,31)) %>% 
    mutate_if(is.character,as.factor)
  
  return(sitadel)
}


# chargement -----------
sitadel<-map_dfr(millesimes,~creer_millesimes(.x)) %>% 
  complete(depcom,date,variable,fill = list(valeur =0)) %>% 
  pivot_wider(names_from = variable,values_from = valeur)


# versement dans le sgbd/datamart.portrait_territoires et metadonnées -------------
source("R/poster_documenter_data.R", encoding = "UTF-8")
poster_documenter_it(df = sitadel ,
                     nom_table_sgbd = "source_sitadel", 
                     comm_source_en_plus = paste0("Chargement des donn\u00e9es sur Geokit3"))


