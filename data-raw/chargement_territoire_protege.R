
# chargement_territoire_protege

library(magrittr)

rm(list = ls())


# chargement des protections nécessaires au calcul ----------
n_enp_apb_s_000 <- datalibaba::importer_data(
  db = "consultation",
  schema = "nature_biodiversite",
  table = "n_enp_apb_s_000"
  )
n_enp_pnr_s_000 <- datalibaba::importer_data(
  db = "consultation",
  schema = "nature_biodiversite",
  table = "n_enp_pnr_s_000"
  ) 
n_enp_rnn_s_000 <- datalibaba::importer_data(
  db = "consultation",
  schema = "nature_biodiversite",
  table = "n_enp_rnn_s_000"
  ) 
n_enp_rnr_s_000 <- datalibaba::importer_data(
  db = "consultation",
  schema = "nature_biodiversite",
  table = "n_enp_rnr_s_000"
  ) 
# Sites classés au titre de la Directive Habitats : périmètres transmis à la CE (ZSC/pSIC/SIC)
n_sic_s_000 <- datalibaba::importer_data(
  db = "consultation",
  schema = "nature_biodiversite",
  table = "n_sic_s_000"
  ) 
# Sites classés au titre de la Directive Habitats : périmètres publiés au JOUE (ZSC/SIC)
n_sic_ue_s_000 <- datalibaba::importer_data(
  db = "consultation",
  schema = "nature_biodiversite",
  table = "n_sic_ue_s_000"
  ) 
n_zps_s_000 <- datalibaba::importer_data(
  db = "consultation",
  schema = "nature_biodiversite",
  table = "n_zps_s_000"
  )

save.image(file = "sauvegarde_intermediaire_territoire_protege.RData")


# limites communales ---------
n_commune_exp_000 <- datalibaba::importer_data(
  db = "referentiels",
  schema = "adminexpress",
  table = "n_commune_exp_000"
  ) 
# ADMIN-EXPRESS édition Mars 2023 France entière au 23/02/2024

# chargement de la liste des depcom COGiter région et région + epci
source("R/levels_facteurs_com.R")

com_epci_r52 <- n_commune_exp_000 %>% 
  dplyr::select(depcom = insee_com) %>% 
  dplyr::filter(depcom %in% com_reg_et_vois) %>% 
  dplyr::mutate(surf_calculee_en_m2 = sf::st_area(the_geom)) # 1289 obs, dont l'Ile d'Yeu (85113)!

# save(com_epci_r52, file = "extdata/com_epci_r52.RData")


# intersection de chaque protection et des communes ---------
# (y compris celles appartenant à un Epci à cheval sur plusieurs régions)
apb <- sf::st_intersection(
  sf::st_buffer(n_enp_apb_s_000, 0),
  sf::st_buffer(com_epci_r52, 0)
  )
apb_2 <- apb %>%
  dplyr::mutate(surf_territoire_protege = sf::st_area(the_geom)) %>%
  sf::st_drop_geometry() %>%
  dplyr::right_join( com_epci_r52 %>% sf::st_drop_geometry()) %>%
  dplyr::mutate(
    surf_territoire_protege = ifelse(is.na(surf_territoire_protege), 0, surf_territoire_protege)
    ) %>% 
  dplyr::group_by(depcom) %>% 
  dplyr::summarise(surf_territoire_protege_apb = sum(surf_territoire_protege)) %>% 
  dplyr::ungroup()
apb_3 <- dplyr::summarise(apb, do_union = TRUE)

pnr <- sf::st_intersection(
  sf::st_buffer(n_enp_pnr_s_000, 0),
  sf::st_buffer(com_epci_r52, 0)
  )
pnr_2 <- pnr %>% 
  dplyr::mutate(surf_territoire_protege = sf::st_area(the_geom)) %>%
  sf::st_drop_geometry() %>%
  dplyr::right_join(com_epci_r52 %>% sf::st_drop_geometry()) %>%
  dplyr::mutate(
    surf_territoire_protege = ifelse(is.na(surf_territoire_protege), 0, surf_territoire_protege)
    ) %>% 
  dplyr::group_by(depcom) %>% 
  dplyr::summarise(surf_territoire_protege_pnr = sum(surf_territoire_protege)) %>% 
  dplyr::ungroup()
pnr_3 <- dplyr::summarise(pnr, do_union = TRUE)

rnn <- sf::st_intersection(
  sf::st_buffer(n_enp_rnn_s_000, 0),
  sf::st_buffer(com_epci_r52, 0)
  )
rnn_2 <- rnn %>% 
  dplyr::mutate(surf_territoire_protege = sf::st_area(the_geom)) %>%
  sf::st_drop_geometry() %>%
  dplyr::right_join(com_epci_r52 %>% sf::st_drop_geometry()) %>%
  dplyr::mutate(
    surf_territoire_protege = ifelse(is.na(surf_territoire_protege), 0, surf_territoire_protege)
    ) %>% 
  dplyr::group_by(depcom) %>% 
  dplyr::summarise(surf_territoire_protege_rnn = sum(surf_territoire_protege)) %>% 
  dplyr::ungroup()
rnn_3 <- dplyr::summarise(rnn, do_union = TRUE)

rnr <- sf::st_intersection(
  sf::st_buffer(n_enp_rnr_s_000, 0),
  sf::st_buffer(com_epci_r52, 0)
  )
rnr_2 <- rnr %>%
  dplyr::mutate(surf_territoire_protege = sf::st_area(the_geom)) %>%
  sf::st_drop_geometry() %>%
  dplyr::right_join(com_epci_r52 %>% sf::st_drop_geometry()) %>%
  dplyr::mutate(
    surf_territoire_protege = ifelse(is.na(surf_territoire_protege), 0, surf_territoire_protege)
    ) %>% 
  dplyr::group_by(depcom) %>% 
  dplyr::summarise(surf_territoire_protege_rnr = sum(surf_territoire_protege)) %>% 
  dplyr::ungroup()
rnr_3 <- dplyr::summarise(rnr, do_union = TRUE)

sic <- sf::st_intersection(
  sf::st_buffer(n_sic_s_000, 0),
  sf::st_buffer(com_epci_r52, 0)
  )
sic_2 <- sic %>% 
  dplyr::mutate(surf_territoire_protege = sf::st_area(the_geom)) %>%
  sf::st_drop_geometry() %>%
  dplyr::right_join(com_epci_r52 %>% sf::st_drop_geometry()) %>%
  dplyr::mutate(
    surf_territoire_protege = ifelse(is.na(surf_territoire_protege), 0, surf_territoire_protege)
    ) %>% 
  dplyr::group_by(depcom) %>% 
  dplyr::summarise(surf_territoire_protege_sic = sum(surf_territoire_protege)) %>% 
  dplyr::ungroup()
sic_3 <- dplyr::summarise(sic, do_union = TRUE)

zps <- sf::st_intersection(
  sf::st_buffer(n_zps_s_000, 0),
  sf::st_buffer(com_epci_r52, 0)
  )
zps_2 <- zps %>%
  dplyr::mutate(surf_territoire_protege = sf::st_area(the_geom)) %>%
  sf::st_drop_geometry() %>%
  dplyr::right_join(com_epci_r52 %>% sf::st_drop_geometry()) %>%
  dplyr::mutate(
    surf_territoire_protege = ifelse(is.na(surf_territoire_protege), 0, surf_territoire_protege)
    ) %>% 
  dplyr::group_by(depcom) %>% 
  dplyr::summarise(surf_territoire_protege_zps = sum(surf_territoire_protege)) %>% 
  dplyr::ungroup()
zps_3 <- dplyr::summarise(zps, do_union = TRUE)

save.image(file = "sauvegarde_intermediaire_territoire_protege.RData")


# historique des modifications géométriques -----------
# 2015 à 2022, date_crea, modif_geo
apb_filter <- apb %>% dplyr::filter(!is.na(modif_geo)) # 4 obs
pnr_filter <- pnr %>% dplyr::filter(!is.na(modif_geo)) # 234 obs
rnn_filter <- rnn %>% dplyr::filter(!is.na(modif_geo)) # 0 obs
rnr_filter <- rnr %>% dplyr::filter(!is.na(modif_geo)) # 5 obs
# pas d'information pour sic
# pas d'information pour zps


# union sans double compte des protections -----------
x <- sf::st_union(
  sf::st_buffer(apb_3, 0), 
  sf::st_buffer(pnr_3, 0)
  )
x <- dplyr::summarise(x, do_union = TRUE)
x <- sf::st_union(
  sf::st_buffer(x, 0), 
  sf::st_buffer(rnn_3, 0)
  )
x <- dplyr::summarise(x, do_union = TRUE)
x <- sf::st_union(
  sf::st_buffer(x, 0),  
  sf::st_buffer(rnr_3, 0)
  )
x <- dplyr::summarise(x, do_union = TRUE)
x <- sf::st_union(
  sf::st_buffer(x, 0), 
  sf::st_buffer(sic_3, 0)
  )
x <- dplyr::summarise(x, do_union = TRUE)
x <- sf::st_union(
  sf::st_buffer(x, 0), 
  sf::st_buffer(zps_3, 0)
  )
territoire_protege_sans_double_compte_r52 <- dplyr::summarise(x, do_union = TRUE)
rm(x)
# mapview::mapview(territoire_protege_sans_double_compte_r52)

save.image(file = "sauvegarde_intermediaire_territoire_protege.RData")


# intersection de l'union des protections et des communes ---------
intersection_territoire_protege_commune <- sf::st_intersection(
  sf::st_buffer(territoire_protege_sans_double_compte_r52, 0),
  sf::st_buffer(com_epci_r52, 0)
  ) %>%
  dplyr::mutate(surf_territoire_protege = sf::st_area(the_geom)) %>%
  sf::st_drop_geometry() %>%
  dplyr::right_join(com_epci_r52 %>% sf::st_drop_geometry()) %>%
  dplyr::mutate(
    surf_territoire_protege = ifelse(is.na(surf_territoire_protege), 0, surf_territoire_protege)
    )
intersection_territoire_protege_commune$surf_calculee_en_m2 <- units::drop_units(intersection_territoire_protege_commune$surf_calculee_en_m2)

save.image(file = "sauvegarde_intermediaire_territoire_protege.RData")


# calcul de l'indicateur -----------
source_territoire_protege_0 <- intersection_territoire_protege_commune %>%
  dplyr::mutate(date = Sys.Date()) %>% 
  # les mises à jour des tables de protections naturelles s'effectuent en continue selon SIGLOIRE
  dplyr::mutate_if(is.character, as.factor) %>% 
  dplyr::select(depcom, date, tidyselect::everything()) %>% 
  dplyr::arrange(date, depcom)


# # pour information ----------
# 
# territoire_protege_r52 <- filter(source_territoire_protege, depcom %in% com_reg)
# pourcentage_region_pdl <- sum(territoire_protege_r52$surf_territoire_protege)/sum(territoire_protege_r52$surf_calculee_en_m2)*100
# # 17.70899 % au 15/03/2023
# sum(territoire_protege_r52$surf_territoire_protege)
# # 5730426625 m² compris l'Ile d'Yeu
# 
# n_region_exp_2020_000_r52 <- datalibaba::importer_data(
#   db = "referentiels",
#   schema = "adminexpress",
#   table = "n_region_exp_2020_000"
#   ) %>%
#   dplyr::filter(insee_reg == 52)
# 
# mapview::mapview(n_region_exp_2020_000_r52, lwd = 3, alpha.regions = 0,legend = F) +
#   mapview::mapview(com_epci_r52, alpha.regions = 0,legend = F) +
#   mapview::mapview(territoire_protege_sans_double_compte_r52)


# détail des protections par communes -----------
protection_com_detail <- dplyr::left_join(apb_2, pnr_2) %>% 
  dplyr::left_join(rnn_2) %>% 
  dplyr::left_join(rnr_2) %>% 
  dplyr::left_join(sic_2) %>% 
  dplyr::left_join(zps_2)

source_territoire_protege <- dplyr::left_join(
  source_territoire_protege_0, 
  protection_com_detail
  )


# versement dans le sgbd/datamart.portrait_territoires -------------
source("R/poster_documenter_data.R", encoding = "UTF-8")
poster_documenter_it(
  df = source_territoire_protege, 
  nom_table_sgbd = "source_territoire_protege", 
  comm_source_en_plus = ""
  )


