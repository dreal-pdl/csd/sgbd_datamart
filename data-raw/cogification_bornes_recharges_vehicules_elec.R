# cogification_bornes_recharges_vehicules_elec
rm(list = ls())

# librairies ------
library(datalibaba)
library(dplyr)
library(COGiter)
source("R/cogifier_it.R")
source("R/poster_doc_post_cogifier.R")

# cogification
cogifier_it("bornes_recharges_vehicules_elec")

# versement dans le sgbd/datamart.portrait_territoires et metadonnee -------------
poster_documenter_post_cogifier(source = "bornes_recharges_vehicules_elec")

rm(list=ls())




