
# cogification_compatibilite_steu_normes_ue

rm(list = ls())

source("R/cogifier_it.R")
source("R/poster_doc_post_cogifier.R")

# cogification et versement sur SGBD
cogifier_it(nom_source = "compatibilite_steu_normes_ue")

# propagation des métadonnées
poster_documenter_post_cogifier(source = "compatibilite_steu_normes_ue")


rm(list=ls())
