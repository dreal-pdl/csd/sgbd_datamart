
# cogification_eco_ptz


# librairies ------
library(DBI)
library(RPostgreSQL)
library(dplyr)
library(COGiter)
library(datalibaba)

rm(list = ls())

source_eco_ptz <- importer_data(db = "datamart",
                             schema = "portrait_territoires",
                             table = "source_eco_ptz")

cogifiee_eco_ptz<-cogifier(source_eco_ptz %>% rename(DEPCOM=depcom))%>% 
  mutate_if(is.factor,as.character)

poster_data(data = cogifiee_eco_ptz,
            db = "datamart",
            schema = "portrait_territoires", 
            table = "cogifiee_eco_ptz",
            pk = c("TypeZone", "Zone", "CodeZone", "date"),
            post_row_name = FALSE, 
            overwrite = TRUE,
            droits_schema = TRUE,
            user = "does")

# commentaires de la table et des variables -------------

# récupération des commentaires de la table source
dico_var <- get_table_comments(
  db = "datamart",
  schema = "portrait_territoires",
  table = "source_eco_ptz",
  user = "does")

# commentaire de la table
comm_table <- filter(dico_var, is.na(nom_col)) %>% 
  pull(commentaire) %>% 
  gsub("\nCommentaire.*$", "", .)

commenter_table(
  comment = comm_table,
  db = "datamart",
  schema = "portrait_territoires",
  table = "cogifiee_eco_ptz",
  user = "does"
)

# commentaire des variables
comm_champ <- select(dico_var, nom_col, commentaire) %>% 
  filter(!is.na(nom_col), nom_col != "depcom") %>% 
  bind_rows(
    tribble(
      ~nom_col, ~commentaire,
      "TypeZone", "Type de territoire",
      "Zone", " Nom du territoire",
      "CodeZone", "Code INSEE du territoire"
    )
  )

post_dico_attr(
  dico = comm_champ,
  db = "datamart",
  schema = "portrait_territoires",
  table = "cogifiee_eco_ptz",
  user = "does"
)
