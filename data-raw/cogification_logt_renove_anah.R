# cogification_logt_renove_anah

rm(list = ls())

# librairies ------
library(datalibaba)
library(dplyr)
library(COGiter)
source("R/cogifier_it.R")
source("R/poster_doc_post_cogifier.R")

# cogification
cogifier_it(nom_source = "logt_renove_anah", metro = FALSE)

# # verif na dans EPCI à cheval
# df_cog <- importer_data(db = "datamart", schema = "portrait_territoires",
#                         table = paste0("cogifiee_", "logt_renove_anah"))

# versement dans le sgbd/datamart.portrait_territoires et metadonnee -------------
poster_documenter_post_cogifier(source = "logt_renove_anah")

rm(list=ls())
