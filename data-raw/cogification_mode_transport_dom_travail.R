
# cogification_mode_transport_dom_travail

rm(list=ls())

source("R/cogifier_it.R")
source("R/poster_doc_post_cogifier.R")

# cogification
cogifier_it(nom_source = "mode_transport_dom_travail")

# versement dans le sgbd/datamart.portrait_territoires et metadonnee -------------
poster_documenter_post_cogifier(source = "mode_transport_dom_travail")

rm(list=ls())
