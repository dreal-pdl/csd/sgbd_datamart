
# cogification_population_legale

rm(list=ls())

source("R/cogifier_it.R")
source("R/poster_doc_post_cogifier.R")

# cogification et versement sur SGBD
cogifier_it(nom_source = "population_legale")

# propagation des métadonnées
poster_documenter_post_cogifier(source = "population_legale")

rm(list=ls())
