
# cogification_sitadel

rm(list=ls())

source("R/cogifier_it.R")
source("R/poster_doc_post_cogifier.R")

# cogification
cogifier_it(nom_source = "sitadel")

# versement dans le sgbd/datamart.portrait_territoires et metadonnee -------------
poster_documenter_post_cogifier(source = "sitadel")

rm(list=ls())

