# cogification_superficie_territoires

rm(list = ls())

# librairies ------

source("R/cogifier_it.R")
source("R/poster_doc_post_cogifier.R")

cogifier_it(nom_source = "superficie_territoires")

poster_documenter_post_cogifier(source = "superficie_territoires")
