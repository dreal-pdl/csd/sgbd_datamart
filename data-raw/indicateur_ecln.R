
# indicateur_ecln

# librairies ---------
library(dplyr)
library(tidyr)
library(DBI)
library(RPostgreSQL)
library(datalibaba)
library(googlesheets4)
library(tidyverse)

rm(list=ls())

# chargement data -------------
secretise_ecln <- importer_data(db = "datamart",
                                schema = "portrait_territoires",
                                table = "secretise_ecln")


# calcul -------
indicateur_ecln<-secretise_ecln %>% 
  mutate(cout_moyen_d_un_logement_vendu_par_les_promoteurs.collectif = round(prix_total_des_ventes.collectif / nb_logt_vendus.collectif,digits = 1)) %>%
  mutate(cout_moyen_d_un_logement_vendu_par_les_promoteurs.individuel = round(prix_total_des_ventes.individuel / nb_logt_vendus.individuel,digits = 1)) %>%
  mutate(cout_moyen_d_un_logement_vendu_par_les_promoteurs.total = round(prix_total_des_ventes.total / nb_logt_vendus.total,digits = 1)) %>%
  
  # mutate(cout_moyen_par_m2_de_SHON_d_un_logement_vendu_par_les_promoteurs.collectif = round(prix_total_des_ventes.collectif / surface_totale_des_ventes.collectif,digits = 1)) %>%
  # mutate(cout_moyen_par_m2_de_SHON_d_un_logement_vendu_par_les_promoteurs.individuel = round(prix_total_des_ventes.individuel / surface_totale_des_ventes.individuel,digits = 1))%>%
  # mutate(cout_moyen_par_m2_de_SHON_d_un_logement_vendu_par_les_promoteurs.total = round(prix_total_des_ventes.total / surface_totale_des_ventes.total,digits = 1))%>%
  
  mutate(cout_moyen_par_m2_de_SHON_logt_vendu_par_promoteurs.collectif = round(prix_total_des_ventes.collectif / surface_totale_des_ventes.collectif,digits = 1)) %>%
  mutate(cout_moyen_par_m2_de_SHON_logt_vendu_par_promoteurs.individuel = round(prix_total_des_ventes.individuel / surface_totale_des_ventes.individuel,digits = 1))%>%
  mutate(cout_moyen_par_m2_de_SHON_logt_vendu_par_promoteurs.total = round(prix_total_des_ventes.total / surface_totale_des_ventes.total,digits = 1))%>%
  
  select(-c(nb_logt_en_cours.collectif:`nb_logt_mis_en_vente.total`)) %>%
  mutate_if(is.factor, as.character)


# versement dans le sgbd/datamart.portrait_territoires -------------
poster_data(data = indicateur_ecln,
            db = "datamart",
            schema = "portrait_territoires", 
            table = "indicateur_ecln",
            pk = c("TypeZone", "Zone", "CodeZone", "date"),
            post_row_name = FALSE, 
            overwrite = TRUE,
            droits_schema = TRUE,
            user = "does")

# METADONNEES------------------------------------

## On récupère la liste des variables qui sont à documenter dans le tableur google sheet à partir du jeu de données posté
var <- setdiff(names(indicateur_ecln), c("TypeZone", "Zone" , "CodeZone" , "date"))

## récupération du nom du présent script source pour filtrer ensuite le référentiel des indicateurs
nom_script_sce <- rstudioapi::getActiveDocumentContext()$path %>% # utilisation de rstudioapi pour récupérer le nom du présent script 
  basename() %>% # on enlève le chemin d'accès pour ne garder que le nom du fichier
  gsub(pattern = ".R$", "", .) # on enlève l'extension '.R'

## authentification google sheet grâce au .Renviron
gs4_auth_configure(api_key = Sys.getenv("google_api_key"))
gs4_deauth()



## chargement du référentiel indicateurs google sheet
metadata_indicateur <- read_sheet("https://docs.google.com/spreadsheets/d/1n-dhtrJM3JwFVz5WSEGOQzQ8A0G7VT_VcxDe5gh6zSo/edit#gid=60292277",
                                  sheet = "indicateurs") %>%
  # on ne garde que les variables concernées par le présent script de chargement
  filter(source == nom_script_sce) %>% 
  # on ajoute l'unité dans le libellé de la variable
  mutate(libelle_variable = paste0(libelle_variable, " (unit\u00e9 : ", unite, ")")) %>% 
  select(variable, libelle_variable) %>% 
  # ajout des libellés pour depcom et date
  bind_rows(
    tribble(
      ~variable, ~libelle_variable,
      "TypeZone", "Type de territoire",
      "Zone", " Nom du territoire",
      "CodeZone", "Code INSEE du territoire",
      "date", "Millesime"
    )
  )

## Vérification que la documentation des indicateurs est complète
all(var %in% metadata_indicateur$variable) # doit renvoyer TRUE

## Envoi des libellés de variable dans le SGBD
post_dico_attr(dico = metadata_indicateur, table = "indicateur_ecln", schema = "portrait_territoires",
               db = "datamart", user = "does")

## Récupération des métadonnées de la source
nom_sce <- str_replace(nom_script_sce, "chargement_|ref_|specifique_", "") %>%
  str_replace("indicateur_", "") %>%
  str_replace("_cogiter|_cog$", "")

metadata_source <- read_sheet("https://docs.google.com/spreadsheets/d/1n-dhtrJM3JwFVz5WSEGOQzQ8A0G7VT_VcxDe5gh6zSo/edit#gid=60292277",
                              sheet = "sources") %>%
  filter(source == nom_sce) %>% 
  mutate(com_table = paste0(source_lib, " - ", producteur, ".\n", descriptif_sources)) %>% 
  pull(com_table) %>%
  # ajout de complement sur la généalogie
  paste0(".\n", "Chargement des donn\u00e9es sur Geokit3")

## commentaires de la table

commenter_table(comment = metadata_source,
                db = "datamart",
                schema = "portrait_territoires",
                table = "indicateur_ecln", 
                user = "does")

rm(list=ls())
