
# indicateur_etat_ecologique_cours_eau

library(magrittr)

rm(list = ls())


cogifiee_etat_ecologique_cours_eau <- datalibaba::importer_data(
  db = "datamart",
  schema = "portrait_territoires",
  table = "cogifiee_etat_ecologique_cours_eau"
  )

indicateur_etat_ecologique_cours_eau <- cogifiee_etat_ecologique_cours_eau %>% 
  dplyr::group_by(TypeZone, Zone, CodeZone, date) %>% 
  dplyr::summarise(
    part_cours_eau_bon_etat = round(surface_bon_etat_masse_eau/surface_terr * 100, digits = 2)
    ) %>% 
  dplyr::ungroup()


# versement dans le sgbd/datamart.portrait_territoires -------------
source("R/poster_documenter_ind.R")
poster_documenter_ind(
  df = indicateur_etat_ecologique_cours_eau, 
  nom_table_sgbd = "indicateur_etat_ecologique_cours_eau", 
  nom_script_sce = "indicateur_etat_ecologique_cours_eau", comm_source_en_plus = ""
  )

rm(list=ls())
