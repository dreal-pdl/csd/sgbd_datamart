# indicateur_pist_cycl_pop_legale

rm(list = ls())

# librairies ---------
library(dplyr)
library(tidyr)
library(lubridate)
library(datalibaba)
source("R/poster_documenter_ind.R")

# chargement data ----------

cogifiee_pistes_cyclables_osm0 <- importer_data(table = "cogifiee_pistes_cyclables_osm", schema = "portrait_territoires", db = "datamart", user = "does")

cogifiee_pistes_cyclables_osm <- pivot_longer(cogifiee_pistes_cyclables_osm0,
                                              cols = where(is.numeric),
                                              names_to = "variable",
                                              values_to = "valeur")

osm <- cogifiee_pistes_cyclables_osm %>% 
  filter(variable == "km_cyclables_tot") %>% 
  pivot_wider(names_from = variable, values_from = valeur) %>% 
  mutate(annee = year(date))

cogifiee_population_legale <- importer_data(table = "cogifiee_population_legale", schema = "portrait_territoires", db = "datamart", user = "does")

population_legale <- cogifiee_population_legale %>% 
  filter(date == max(date)) %>% 
  select(-date)


# calcul ----------
indic_lg_cycl_par_hab <- left_join(osm, population_legale, by = c("TypeZone", "Zone", "CodeZone")) %>% 
  mutate(metre_cycl_par_hab = round(km_cyclables_tot * 1000 / population_municipale, digits = 2)) %>% # conversion en m
  select(-c(annee, km_cyclables_tot, population_municipale)) %>% 
  mutate_if(is.factor, as.character)


# chargement dans le SGBD des indicateurs calculés et des commentaires -------------
poster_documenter_ind(df = indic_lg_cycl_par_hab, nom_table_sgbd = "indicateur_pist_cycl_pop_legale", 
                      comm_source_en_plus = "", nom_script_sce = "indicateur_pist_cycl_pop_legale")

rm(list=ls())
