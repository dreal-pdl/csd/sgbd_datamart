
# indicateur_rpls

# librairies ----------
library(dplyr)
library(tidyr)
library(datalibaba)

rm(list = ls())


# chargement data ---------------
cogifiee_rpls <- importer_data(table = "cogifiee_rpls",
                               schema = "portrait_territoires",
                               db = "datamart",
                               user = "does")


# calcul ----------
indicateur_rpls <- cogifiee_rpls %>% 
  select(TypeZone, Zone, CodeZone, date, loyer_moy_hlm_surf_hab = loyer_m2, 
         taux_mobilite_parc_hlm = taux_mobilite, contains("nb_ls_dpe_ener_")) %>% 
  mutate(nb_ls_energ = (nb_ls_dpe_ener_E + nb_ls_dpe_ener_F + nb_ls_dpe_ener_G),
         nb_ls_dpe_renseigne = nb_ls_dpe_ener_A + nb_ls_dpe_ener_B + nb_ls_dpe_ener_C + nb_ls_dpe_ener_D + nb_ls_energ,
         proportion_ls_energ = round(nb_ls_energ / nb_ls_dpe_renseigne * 100, digits = 1),
         nb_logements_sociaux_passoires_thermiques = nb_ls_dpe_ener_F + nb_ls_dpe_ener_G,
         part_logements_sociaux_passoires_thermiques = round(nb_logements_sociaux_passoires_thermiques / nb_ls_dpe_renseigne *100, digits = 1),
         across(where(is.character), as.factor))  %>%
  select(TypeZone, Zone, CodeZone, date, loyer_moy_hlm_surf_hab, taux_mobilite_parc_hlm, nb_ls_energ, proportion_ls_energ,
         nb_logements_sociaux_passoires_thermiques, part_logements_sociaux_passoires_thermiques) %>% 
  mutate(part_logements_sociaux_passoires_thermiques = na_if(part_logements_sociaux_passoires_thermiques, NaN),
         proportion_ls_energ = na_if(proportion_ls_energ, NaN),
         across(where(is.factor), as.character))


# versement dans le sgbd/datamart.portrait_territoiresdes données et de leur métadonnées -------------
source("R/poster_documenter_ind.R", encoding = "UTF-8")
poster_documenter_ind(df = indicateur_rpls, 
                      nom_table_sgbd = "indicateur_rpls", 
                      comm_source_en_plus = "")

rm(list=ls())



