
# indicateur_statut d'occupation des rp
# librairies ----------
library(datalibaba)
library(tidyverse)

rm(list = ls())


# chargement data ---------------
cogifiee_statut_occupation_des_rp <- importer_data(table = "cogifiee_statut_occupation_des_rp",
                                        schema = "portrait_territoires",
                                        db = "datamart",
                                        user = "does")
# calcul ----------
indicateur_statut_occupation_des_rp <- cogifiee_statut_occupation_des_rp %>%
  mutate(nb_rp_locataire_total = nb_rp_locataire_hlm_1a2_pieces + nb_rp_locataire_hlm_3a4_pieces
         + nb_rp_locataire_hlm_5_pieces_et_plus + nb_rp_locataire_non_hlm_1a2_pieces
         + nb_rp_locataire_non_hlm_3a4_pieces + nb_rp_locataire_non_hlm_5_pieces_et_plus,
         nb_rp_proprietaire_total = nb_rp_proprietaire_1a2_pieces + nb_rp_proprietaire_3a4_pieces + nb_rp_proprietaire_5_pieces_et_plus,
         nb_rp_gratuit_total = nb_rp_gratuit_1a2_pieces + nb_rp_gratuit_3a4_pieces + nb_rp_gratuit_5_pieces_et_plus,
         nb_rp_total = nb_rp_locataire_total + nb_rp_proprietaire_total + nb_rp_gratuit_total) %>% 
  mutate(part_locataires_dans_parc_rp = nb_rp_locataire_total / nb_rp_total *100,
         part_proprietaires_dans_parc_rp = nb_rp_proprietaire_total / nb_rp_total *100,
         part_gratuits_dans_parc_rp = nb_rp_gratuit_total / nb_rp_total *100,
         part_locataires_hlm_dans_parc_rp = (nb_rp_locataire_hlm_1a2_pieces + nb_rp_locataire_hlm_3a4_pieces
                                             + nb_rp_locataire_hlm_5_pieces_et_plus) / nb_rp_total *100,
         part_locataires_non_hlm_dans_parc_rp = (nb_rp_locataire_non_hlm_1a2_pieces + nb_rp_locataire_non_hlm_3a4_pieces 
                                                 + nb_rp_locataire_non_hlm_5_pieces_et_plus)/ nb_rp_total *100) %>% 
  mutate(part_locataires_1a2_pieces = (nb_rp_locataire_hlm_1a2_pieces + nb_rp_locataire_non_hlm_1a2_pieces)/nb_rp_locataire_total*100,
         part_locataires_3a4_pieces = (nb_rp_locataire_hlm_3a4_pieces + nb_rp_locataire_non_hlm_3a4_pieces)/nb_rp_locataire_total*100,
         part_locataires_5_pieces_et_plus = (nb_rp_locataire_hlm_5_pieces_et_plus + nb_rp_locataire_non_hlm_5_pieces_et_plus)/nb_rp_locataire_total*100
         ) %>%
  select(TypeZone,Zone,CodeZone,date,
         part_locataires_1a2_pieces,part_locataires_3a4_pieces,part_locataires_5_pieces_et_plus,
         nb_rp_locataire_total,nb_rp_proprietaire_total,nb_rp_gratuit_total,part_locataires_hlm_dans_parc_rp,part_locataires_non_hlm_dans_parc_rp,
         part_locataires_dans_parc_rp,part_proprietaires_dans_parc_rp,part_gratuits_dans_parc_rp
         ) %>% 
  arrange(TypeZone, Zone, CodeZone, date)
 
# versement dans le sgbd/datamart.portrait_territoiresdes données et de leur métadonnées -------------
source("R/poster_documenter_ind.R", encoding = "UTF-8")
poster_documenter_ind(df = indicateur_statut_occupation_des_rp, nom_table_sgbd = "indicateur_statut_occupation_des_rp", 
                      comm_source_en_plus = "")

rm(list=ls())




