
# secret_eptb

# librairies -----------
library(dplyr)
library(tidyr)
library(lubridate)
library(COGiter)
library(stringr)
library(DBI)
library(RPostgreSQL)
library(datalibaba)
library(googlesheets4)

rm(list=ls())


# chargement data -------------

cogifiee_eptb <- importer_data(db = "datamart",
                             schema = "portrait_territoires",
                             table = "cogifiee_eptb")


data_cogifiee<-pivot_longer(cogifiee_eptb,
                            cols = nombre_d_observations_non_pondere : surface_totale_des_maisons_en_m2,
                            names_to = "variable",
                            values_to = "valeur")

# tranches_eptb <- read.csv2('metadata/secret_eptb.csv', as.is = TRUE, encoding = "UTF-8" )

## authentification google sheet grâce au .Renviron
gs4_auth_configure(api_key = Sys.getenv("google_api_key"))
gs4_deauth()

## chargement du référentiel indicateurs google sheet
tranches_eptb <- read_sheet("https://docs.google.com/spreadsheets/d/1n-dhtrJM3JwFVz5WSEGOQzQ8A0G7VT_VcxDe5gh6zSo/edit#gid=60292277",
                                  sheet = "secret_eptb")


# distinction des données à secrétiser ---------

# table des variables nombre
data_nombres <- data_cogifiee  %>%
  filter ( str_detect(variable,"^nombre")==TRUE)

#table des variables prix et surfaces
data_prix_surf <- data_cogifiee  %>%
  filter ( str_detect(variable,"^nombre")==FALSE)

# secrétisation des données nombre

# Communes
secret_communes<-data_nombres %>% 
  filter(TypeZone=="Communes")%>%
  left_join(communes %>% rename("CodeZone"="DEPCOM")%>% select(CodeZone,EPCI)) #ajoute les noms d'epci

#secret induit, pour une meme variable et un meme epci, pour ne pas retrouver la valeur d'une commune
#en faisant la somme des communes
secret_communes<-secret_communes %>% 
  group_by(variable,EPCI,date) %>%
  mutate(A_nb_inf11=length(which(valeur<11)),           #compte combien inférieur à 11
         A_rang=rank(valeur, ties.method = "first"),    #classe pour repérer les 2 plus petites valeurs
         valeur2 = valeur)

secret_communes<-secret_communes %>%
  mutate(valeur=as.character(valeur))

secret_communes$valeur<-case_when(
  secret_communes$valeur2<11 ~ "nc",      #secret pour toutes les nb_obs inférieurs à 11
  secret_communes$A_rang<3 ~ "nc",         #secret sur les 2 communes avec valeurs les plus basses
  TRUE ~ secret_communes$valeur)

secret_communes<-secret_communes %>%
  ungroup() %>%
  select(TypeZone,Zone,CodeZone,date,variable,valeur)

# autres zones (pas de secret induit calculé)
secret_grandes_zones<-data_nombres %>% 
  filter(TypeZone %in% c("Epci","Départements","Régions","France")) %>%
  mutate(valeur2 = valeur,valeur=as.character(valeur))
secret_grandes_zones$valeur <- case_when(
  secret_grandes_zones$valeur2 < 11 ~ "nc",         #secret sur les 2 communes avec valeurs les plus basses
  TRUE ~ secret_grandes_zones$valeur)
secret_grandes_zones<- secret_grandes_zones %>%
  ungroup() %>%
  select(TypeZone,Zone,CodeZone,date,variable,valeur)  

# regroupement des zonages
data_nombres_secretise<-bind_rows(secret_communes,secret_grandes_zones) %>%
  mutate(code_ident=paste(CodeZone,date,variable))  #création d'une colonne pour identifier zone, année et variable


# secrétisation des données prix et surface ----------
data_prix_surf$variable_associe<- tranches_eptb$variable_associee[match(data_prix_surf$variable,tranches_eptb$variable)]

data_prix_surf <- data_prix_surf %>%
  mutate(code_ident=paste(CodeZone,date,variable_associe))  #création d'une colonne pour identifier zone, année et variable
data_prix_surf$valeur_associe <- data_nombres_secretise$valeur[match(data_prix_surf$code_ident,data_nombres_secretise$code_ident)]
data_prix_surf<-data_prix_surf %>%
  mutate(valeur=as.character(valeur))
data_prix_surf$valeur <- case_when(
  data_prix_surf$valeur_associe=="nc" ~ "nc",   #secretise si la valeur associee est secretisee
  TRUE ~ data_prix_surf$valeur)
data_prix_surf<-data_prix_surf %>%
  select(TypeZone,Zone,CodeZone,date,variable,valeur)


# regroupement des donnees ----------- 
data_eptb<-bind_rows(data_nombres_secretise %>% select(-code_ident),data_prix_surf) 

data_eptb$valeur<-na_if(data_eptb$valeur,"nc")  #remplace "nc" par NA   

data_cogifiee <- data_eptb %>%
  mutate(valeur=as.numeric(valeur)) %>%
  mutate_if(is.factor,as.character) 

secretise_eptb<-data_cogifiee %>% 
  pivot_wider(names_from = variable,values_from = valeur)


# versement dans le sgbd/datamart.portrait_territoires -------------
poster_data(data = secretise_eptb,
            db = "datamart",
            schema = "portrait_territoires", 
            table = "secretise_eptb",
            pk = c("TypeZone", "Zone", "CodeZone", "date"),
            post_row_name = FALSE, 
            overwrite = TRUE,
            droits_schema = TRUE,
            user = "does")

# commentaires de la table et des variables -------------

# récupération des commentaires de la table source
dico_var <- get_table_comments(
  db = "datamart",
  schema = "portrait_territoires",
  table = "cogifiee_eptb",
  user = "does")

# commentaire de la table
comm_table <- filter(dico_var, is.na(nom_col)) %>% 
  pull(commentaire) %>% 
  gsub("\nCommentaire.*$", "", .)

commenter_table(
  comment = comm_table,
  db = "datamart",
  schema = "portrait_territoires",
  table = "secretise_eptb",
  user = "does"
)

# commentaire des variables
comm_champ <- select(dico_var, nom_col, commentaire) %>% 
  filter(!is.na(nom_col), nom_col != "depcom") %>% 
  bind_rows(
    tribble(
      ~nom_col, ~commentaire,
      "TypeZone", "Type de territoire",
      "Zone", " Nom du territoire",
      "CodeZone", "Code INSEE du territoire"
    )
  )

post_dico_attr(
  dico = comm_champ,
  db = "datamart",
  schema = "portrait_territoires",
  table = "secretise_eptb",
  user = "does"
)
